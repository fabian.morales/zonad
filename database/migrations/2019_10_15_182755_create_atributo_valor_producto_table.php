<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtributoValorProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atributo_valor_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('producto_id');
            $table->unsignedBigInteger('atributo_id');
            $table->unsignedBigInteger('atributo_valor_id');
            $table->foreign('producto_id')->references('id')->on('producto');
            $table->foreign('atributo_id')->references('id')->on('atributo');
            $table->foreign('atributo_valor_id')->references('id')->on('atributo_valor');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atributo_producto');
    }
}
