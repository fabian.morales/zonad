<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->bigIncrements('id');
            //'titulo', 'peso', 'id_seccion', 'url', 'llave', 'mostrar', 'descripcion', 'meta', 'id_padre'
            $table->string('titulo');
            $table->unsignedSmallInteger('peso');
            $table->string('url')->nullable(true);
            $table->string('slug')->nullable(true);
            $table->string('mostrar', 1);
            $table->string('descripcion')->nullable(true);
            $table->string('meta')->nullable(true);
            $table->unsignedBigInteger('menu_id')->nullable(true);
            $table->nullableMorphs('link');
            $table->timestamps();
            $table->foreign('menu_id')->references('id')->on('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_menu');
    }
}
