<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionAtributoValorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizacion_atributo_valor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cotizacion_id');
            $table->unsignedBigInteger('atributo_valor_id');
            $table->timestamps();
            $table->foreign('cotizacion_id')->references('id')->on('cotizacion');
            $table->foreign('atributo_valor_id')->references('id')->on('atributo_valor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizacion_atributo_valor');
    }
}
