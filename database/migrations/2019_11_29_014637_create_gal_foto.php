<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalFoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gal_foto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('galeria_id');
            $table->string('titulo', 100);
            $table->string('descripcion', 500);
            $table->string('archivo', 100);

            $table->timestamps();            
            $table->foreign('galeria_id')->references('id')->on('gal_galeria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gal_foto');
    }
}
