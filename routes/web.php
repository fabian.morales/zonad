<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('front::home');
Route::get('/nosotros', 'HomeController@mostrarNosotros')->name('front::nosotros');
Route::get('/politica-de-privacidad', 'HomeController@mostrarTYC')->name('front::tyc');
Route::get('/contacto', 'ContactoController@mostrarContacto')->name('front::contacto');
Route::post('/contacto', 'ContactoController@registrar')->name('front::contacto::post');

Route::post('/cotizacion', 'CotizacionController@recibirCotizacion')->name('front::cot');
Route::post('/boletin', 'BoletinController@registrar')->name('front::boletin');

Route::get('/crear', function () {
    $usuario = new App\User();
    $usuario->email = 'admin@correo.com';
    $usuario->nombre = 'Administrador';
    $usuario->password = Hash::make('123456');
    $usuario->save();
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['prefix' => 'productos'], function () {
    Route::get('/{id}-{slug}', 'CategoriaController@mostrarProductos')->name('front::cat');
    Route::get('/{id_cat}-{slug_cat}/{id}-{slug}', 'ProductoController@mostrarProducto')->name('front::prod');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {
    Route::get('/', function () {
        return view('index_admin');
    })->name('admin::index');

    Route::group(['prefix' => 'caracteristica'], function() {
        Route::get('/', 'admin\CaracteristicaController@mostrarIndex')->name('admin::carac::index');
        Route::get('/nuevo', 'admin\CaracteristicaController@mostrarForm')->name('admin::carac::nuevo');
        Route::get('/editar/{id}', 'admin\CaracteristicaController@editar')->name('admin::carac::editar');
        Route::post('/guardar', 'admin\CaracteristicaController@guardar')->name('admin::carac::guardar');
        Route::get('/borrar/{id}', 'admin\CaracteristicaController@borrar')->name('admin::carac::borrar');
    });

    Route::group(['prefix' => 'atributo'], function() {
        Route::get('/', 'admin\AtributoController@mostrarIndex')->name('admin::atrib::index');
        Route::get('/nuevo', 'admin\AtributoController@mostrarForm')->name('admin::atrib::nuevo');
        Route::get('/editar/{id}', 'admin\AtributoController@editar')->name('admin::atrib::editar');
        Route::post('/guardar', 'admin\AtributoController@guardar')->name('admin::atrib::guardar');
        Route::get('/borrar/{id}', 'admin\AtributoController@borrar')->name('admin::atrib::borrar');

        Route::get('/{id}/valores', 'admin\AtributoController@obtenerListaValores')->name('admin::atrib_valor::index');
        Route::get('/valores/{id}', 'admin\AtributoController@mostrarValor')->name('admin::atrib_valor::mostrar');
        Route::post('/valores/guardar', 'admin\AtributoController@guardarValor')->name('admin::atrib_valor::guardar');
        Route::get('/valores/borrar/{id}', 'admin\AtributoController@borrarValor')->name('admin::atrib_valor::borrar');
    });

    Route::group(['prefix' => 'categoria'], function() {
        Route::get('/', 'admin\CategoriaController@mostrarIndex')->name('admin::categ::index');
        Route::get('/{id}', 'admin\CategoriaController@mostrarIndex')->name('admin::categ::mostrar')->where('id', '[0-9]+');
        Route::get('/nuevo', 'admin\CategoriaController@mostrarForm')->name('admin::categ::nuevo');
        Route::get('/editar/{id}', 'admin\CategoriaController@editar')->name('admin::categ::editar');
        Route::post('/guardar', 'admin\CategoriaController@guardar')->name('admin::categ::guardar');
        Route::get('/borrar/{id}', 'admin\CategoriaController@borrar')->name('admin::categ::borrar');
    });

    Route::group(['prefix' => 'categoria-blog'], function() {
        Route::get('/', 'admin\CategoriaBlogController@mostrarIndex')->name('admin::categ_blog::index');
        Route::get('/{id}', 'admin\CategoriaBlogController@mostrarIndex')->name('admin::categ_blog::mostrar')->where('id', '[0-9]+');
        Route::get('/nuevo', 'admin\CategoriaBlogController@mostrarForm')->name('admin::categ_blog::nuevo');
        Route::get('/editar/{id}', 'admin\CategoriaBlogController@editar')->name('admin::categ_blog::editar');
        Route::post('/guardar', 'admin\CategoriaBlogController@guardar')->name('admin::categ_blog::guardar');
        Route::get('/borrar/{id}', 'admin\CategoriaBlogController@borrar')->name('admin::categ_blog::borrar');
    });

    Route::group(['prefix' => 'producto'], function() {
        Route::get('/', 'admin\ProductoController@mostrarIndex')->name('admin::prod::index');
        Route::get('/nuevo', 'admin\ProductoController@mostrarForm')->name('admin::prod::nuevo');
        Route::get('/editar/{id}', 'admin\ProductoController@editar')->name('admin::prod::editar');
        Route::post('/guardar', 'admin\ProductoController@guardar')->name('admin::prod::guardar');
        Route::get('/borrar/{id}', 'admin\ProductoController@borrar')->name('admin::prod::borrar');
        Route::post('/imagenes/{id}', 'admin\ProductoController@guardarImagen')->name('admin::prod::img_add')->where('id', '[0-9]+');;;
        Route::post('/imagenes/{id}/borrar', 'admin\ProductoController@borrarImagenes')->name('admin::prod::img_del');

        Route::get('/{id}/atributos', 'admin\ProductoController@obtenerListaAtributos')->name('admin::prod_atrib::index');
        Route::post('/atributos/guardar', 'admin\ProductoController@guardarAtributo')->name('admin::prod_atrib::guardar');
        Route::get('/atributos/borrar/{id}', 'admin\ProductoController@borrarAtributo')->name('admin::prod_atrib::borrar');
    });

    Route::group(['prefix' => 'contenido'], function() {
        Route::get('/', 'admin\ContenidoController@mostrarIndex')->name('admin::contenido::index');
        Route::get('/nuevo', 'admin\ContenidoController@mostrarForm')->name('admin::contenido::nuevo');
        Route::get('/editar/{id}', 'admin\ContenidoController@editar')->name('admin::contenido::editar');
        Route::post('/guardar', 'admin\ContenidoController@guardar')->name('admin::contenido::guardar');
        Route::get('/borrar/{id}', 'admin\ContenidoController@borrar')->name('admin::contenido::borrar');
    });

    Route::group(['prefix' => 'blog'], function() {
        Route::get('/', 'admin\BlogController@mostrarIndex')->name('admin::blog::index');
        Route::get('/nuevo', 'admin\BlogController@mostrarForm')->name('admin::blog::nuevo');
        Route::get('/editar/{id}', 'admin\BlogController@editar')->name('admin::blog::editar');
        Route::post('/guardar', 'admin\BlogController@guardar')->name('admin::blog::guardar');
        Route::get('/borrar/{id}', 'admin\BlogController@borrar')->name('admin::blog::borrar');
    });

    Route::group(['prefix' => 'cotizacion'], function() {
        Route::get('/', 'admin\CotizacionController@mostrarIndex')->name('admin::cot::index');
        Route::get('/editar/{id}', 'admin\CotizacionController@mostrar')->name('admin::cot::editar');
        Route::post('/guardar', 'admin\CotizacionController@guardar')->name('admin::cot::guardar');
        Route::get('/borrar/{id}', 'admin\CotizacionController@borrar')->name('admin::cot::borrar');
    });

    Route::group(['prefix' => 'galeria'], function() {
        Route::get('/', 'admin\GaleriaController@mostrarIndex')->name('admin::gal::index');
        Route::get('/nuevo', 'admin\GaleriaController@mostrarForm')->name('admin::gal::nuevo');
        Route::get('/editar/{id}', 'admin\GaleriaController@editar')->name('admin::gal::editar');
        Route::post('/guardar', 'admin\GaleriaController@guardar')->name('admin::gal::guardar');
        Route::get('/borrar/{id}', 'admin\GaleriaController@borrar')->name('admin::gal::borrar');

        Route::group(['prefix' => 'foto'], function() {
            Route::post('/guardar', 'admin\GaleriaController@guardarFoto')->name('admin::galfoto::guardar');
            Route::get('/editar/{id}', 'admin\GaleriaController@editarFoto')->name('admin::galfoto::editar');
            Route::get('/borrar/{id}', 'admin\GaleriaController@borrarFoto')->name('admin::galfoto::borrar');
        });
    });

    Route::group(['prefix' => 'boletin'], function() {
        Route::get('/', 'admin\BoletinController@mostrarIndex')->name('admin::bol::index');
        Route::get('/descargar', 'admin\BoletinController@descargarListado')->name('admin::bol::descarga');
    });

    Route::group(['prefix' => 'contacto'], function() {
        Route::get('/', 'admin\ContactoController@mostrarIndex')->name('admin::cont::index');
        Route::get('/descargar', 'admin\ContactoController@descargarListado')->name('admin::cont::descarga');
    });
});

Route::group(['prefix' => 'blog'], function () {
    Route::get('/', 'ContenidoController@mostrarBlogHome')->name('front::blog::home');
    Route::get('/categoria/{id}-{slug}', 'ContenidoController@mostrarBlogCategoria')->name('front::blog::cat');
    Route::get('/articulo/{id}-{slug}', 'ContenidoController@mostrarBlogArticulo')->name('front::blog::art');

    /*Route::get('/{id}-{slug}', 'CategoriaController@mostrarProductos')->name('front::cat');
    Route::get('/{id_cat}-{slug_cat}/{id}-{slug}', 'ProductoController@mostrarProducto')->name('front::prod');*/
});

Route::any('/{key}', 'ContenidoController@mostrarPagina');
