<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaracteristicaProducto extends Model
{
    protected $table = 'caracteristica_producto';

    protected $fillable = [
        'producto_id', 'caracteristica_id', 'valor',
    ];

    public function producto() {
        return $this->belongsTo(Producto::class);
    }

    public function caracteristica() {
        return $this->belongsTo(Caracteristica::class);
    }
}
