<?php

namespace App;

class Galeria extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'gal_galeria';
    protected $fillable = [
        'titulo', 'llave', 'tipo'
    ];
    
    public function fotos(){
        return $this->hasMany(Foto::class);
    }
}
