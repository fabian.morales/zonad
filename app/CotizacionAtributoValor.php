<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CotizacionAtributoValor extends Model
{
    protected $table = 'cotizacion_atributo_valor';

    public function producto() {
        return $this->belongsTo(Cotizacion::class);
    }

    public function atributoValor() {
        return $this->belongsTo(AtributoValor::class);
    }
}
