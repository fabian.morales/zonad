<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contenido extends Model
{
    use SoftDeletes;
    
    protected $table = 'contenido';
    protected $fillable = [
        'slug', 'titulo', 'tipo', 'cuerpo', 'peso', 'inicio', 'activo'
    ];

    public function links() {
        return $this->morphMany(Menu::class, 'link');
    }
}
