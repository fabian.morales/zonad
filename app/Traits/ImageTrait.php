<?php

namespace App\Traits;

trait ImageTrait{
    private $imgBasePath = 'storage/imagenes/';

    public function getOriginalImageUrl() {
        return $this->getImageUrl();
    }

    public function getOriginalImagePath() {
        return $this->getImagePath();
    }

    public function getImagePath($sizeName = "") {
        if (!empty($sizeName)) {
            $sizeName .= "_";
        }

        $ret = public_path($this->imgBasePath . $this->folderName);
        $idField = $this->idField;
        $fileField = $this->fileField;
        if (!empty($this->$idField) && (int)$this->$idField > 0) {
            $ret .= '/' . $this->$idField . '/' . $sizeName . $this->$fileField;
        }

        return $ret;
    }

    public function getImageUrl($sizeName = "") {
        if (!empty($sizeName)) {
            $sizeName .= "_";
        }

        $ret = $this->imgBasePath . $this->folderName;
        $idField = $this->idField;
        $fileField = $this->fileField;

        if (!empty($this->$idField) && (int)$this->$idField > 0) {
            $ret .= '/' . $this->$idField . '/' . $sizeName . $this->$fileField;
        }

        return $ret;
    }
}