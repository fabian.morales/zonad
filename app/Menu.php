<?php

namespace App;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'pagina' => 'App\Contenido',
    'categoria' => 'App\Categoria',
    'blog' => 'App\Blog',
    'categoria_blog' => 'App\CategoriaBlog',
]);

class Menu extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'menu';
    
    protected $fillable = [
        'titulo', 'peso', 'id_seccion', 'url', 'slug', 'mostrar', 'descripcion', 'meta', 'menu_id', 'link_type', 'link_id'
    ];
    
    public function padre(){
        return $this->belongsTo(Menu::class);
    }
    
    public function hijos(){
        return $this->hasMany(Menu::class);
    }

    public function link() {
        return $this->morphTo();
    }
}