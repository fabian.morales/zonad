<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaBlog extends Model
{
    protected $table = 'categoria_blog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'slug'
    ];

    public function posts() {
        return $this->belongsToMany(Blog::class, 'contenido_categoria');
    }

    public function links() {
        return $this->morphMany(Menu::class, 'link');
    }
}
