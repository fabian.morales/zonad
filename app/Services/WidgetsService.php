<?php
namespace App\Services;

use App\Categoria;
use App\CategoriaBlog;
use App\Galeria;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class WidgetsService {
    public function renderGallery($name) {
        $galeria = Galeria::with("fotos")->where("llave", $name)->first();
        return view('content.galeria', ["galeria" => $galeria])->render();
    }

    public function makeMenu() {
        $menus = \App\Menu::with(['link'])->where('mostrar','S')->whereNull('menu_id')->get();
        $categoryCount = $menus->filter(function($item) {
            return $item->link_type == 'categoria';
        })->count();
        $categories = [];
        if ($categoryCount > 0) {
            $categories = Categoria::with(
                [
                    'hijos' => function($q) {
                        $q->whereHas('hijos')->orwhereHas('productos');
                    },
                    'hijos.hijos' => function($q) {
                        $q->whereHas('hijos')->orwhereHas('productos');
                    }
                ])
                ->whereHas('hijos')
                ->orwhereHas('productos')
                ->get()
                ->keyBy('id')
                ->all();
        }

        $ret = [];
        foreach ($menus as $menu) {
            $item = new \stdClass();
            $item->titulo = $menu->titulo;
            $item->link = '';
    
            if (!empty($menu->url)) {
                $item->link = $menu->url;
            }
            elseif (!empty($menu->slug)) {
                $item->link = url($menu->slug);
            }
            elseif ($menu->link_type == 'categoria') {
                if (!key_exists($menu->link_id, $categories)) {
                    continue;
                }
                $category = $categories[$menu->link_id];
                $item->link = route('front::cat', ['id' => $category->id, 'slug' => $category->slug]);
                $item->hijos = [];
                
                foreach ($category->hijos as $hijo) {
                    $itemHijo = new \stdClass();
                    $itemHijo->titulo = $hijo->nombre;
                    $itemHijo->link = route('front::cat', ['id' => $hijo->id, 'slug' => $hijo->slug]);
    
                    if (sizeof($hijo->hijos)) {
                        $itemHijo->hijos = [];
                        foreach ($hijo->hijos as $nieto) {
                            $itemNieto = new \stdClass();
                            $itemNieto->titulo = $nieto->nombre;
                            $itemNieto->link = route('front::cat', ['id' => $nieto->id, 'slug' => $nieto->slug]);
    
                            $itemHijo->hijos[] = $itemNieto;
                        }
                    }
    
                    $item->hijos[] = $itemHijo;
                }
            }
            elseif ($menu->link_type == 'pagina') {
                $item->link = url($menu->link->slug);
            }
    
            $ret[] = $item;
        }
        
        return $ret;
    }

    public function getBlogCategories() {
        $categorias = CategoriaBlog::orderBy('nombre')->get();
        $ret = [];
        if (!empty($categorias)) {
            $ret = $categorias;
        }

        return $ret;
    }
}