<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'correo', 'celular', 'mensaje', 'whatsapp', 'producto_id', 'ciudad', 'acepta_terminos'
    ];

    protected $table = 'cotizacion';

    public function producto() {
        return $this->belongsTo(Producto::class);
    }

    public function valores() {
        return $this->hasMany(CotizacionAtributoValor::class);
    }
}
