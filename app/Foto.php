<?php

namespace App;

class Foto extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'gal_foto';
    protected $fillable = [
        'galeria_id', 'titulo', 'descripcion', 'archivo'
    ];
    
    public function galeria(){
        return $this->belongsTo(Galeria::class);
    }
}
