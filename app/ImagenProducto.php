<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenProducto extends Model
{
    protected $table = 'producto_imagen';

    public function producto() {
        return $this->belongsTo(Producto::class);
    }

    public function getPath() {
        $ret = public_path('storage/imagenes/productos');
        if (!empty($this->producto_id) && (int)$this->producto_id > 0) {
            $ret .= '/' . $this->producto_id . '/' . $this->archivo;
        }

        return $ret;
    }

    public function getThumbPath() {
        $ret = public_path('storage/imagenes/productos');
        if (!empty($this->producto_id) && (int)$this->producto_id > 0) {
            $ret .= '/' . $this->producto_id . '/thumb/' . $this->archivo;
        }

        return $ret;
    }

    public function getUrl() {
        $ret = 'storage/imagenes/productos';
        if (!empty($this->producto_id) && (int)$this->producto_id > 0) {
            $ret .= '/' . $this->producto_id . '/' . $this->archivo;
        }

        return $ret;
    }

    public function getThumbUrl() {
        $ret = 'storage/imagenes/productos';
        if (!empty($this->producto_id) && (int)$this->producto_id > 0) {
            $ret .= '/' . $this->producto_id . '/thumb/' . $this->archivo;
        }

        return $ret;
    }
}
