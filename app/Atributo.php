<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atributo extends Model
{
    protected $table = 'atributo';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'tipo',
    ];

    public function valores() {
        return $this->hasMany(AtributoValor::class);
    }

    public function valoresProducto() {
        return $this->belongsToMany(AtributoValor::class, 'atributo_valor_producto')->withPivot('producto_id', 'id');
    }

    public function productos() {
        return $this->belongsToMany(Producto::class, 'atributo_valor_producto');
    }

    public static function obtenerTipos(){
        return [
            'lista' => 'Lista desplegable',
            'radio' => 'Opciones de radio',
            'color' => 'Selector de colores',
        ];
    }
}
