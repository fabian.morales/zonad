<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'producto';
    private $link;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'slug', 'codigo', 'descripcion', 'destacado', 'oferta', 'descripcion_oferta', 'activo', 'en_stock'
    ];

    public function scopeActivo($query) {
        return $query->where('activo', 'S');
    }

    public function categorias() {
        return $this
            ->belongsToMany(Categoria::class, 'producto_categoria')
            ->withPivot('categoria_id', 'principal');
    }

    public function imagenes() {
        return $this->hasMany(ImagenProducto::class);
    }

    public function atributos(){
        return $this->belongsToMany(Atributo::class, 'atributo_valor_producto')->distinct();
    }

    public function atributosValor() {
        return $this->belongsToMany(AtributoValor::class, 'atributo_valor_producto');
    }

    public function caracteristicas() {
        return $this->hasMany(CaracteristicaProducto::class);
    }

    public function obtenerLink() {
        if (empty($this->link)) {
            $catSlug = '';
            $catId = '';
            if (sizeof($this->categorias)) {
                $categoria = $this->categorias[0];

                if (sizeof($this->categorias) > 1) {
                    $categoria = $this->categorias->first(function($valor) {
                        return $valor->pivot->principal == 1;
                    }) ?? $categoria;
                }

                $catSlug = $categoria->slug;
                $catId = $categoria->id;
            }
    
            $opc = [
                'id' => $this->id,
                'slug' => $this->slug,
                'id_cat' => $catId, 
                'slug_cat' => $catSlug
            ];
            $this->link = route('front::prod', $opc);
        }

        return $this->link;
    }
}
