<?php

namespace App\Http\Controllers;

use App\Blog;
use App\CategoriaBlog;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;

class ContenidoController extends Controller
{
    use \App\ContenidoTrait;

    public function mostrarPagina(Request $request, $key, $id = ''){
        if (empty($key)){
            return redirect()->route('front::home');
        }

        $pagina = \App\Contenido::where("slug", $key)->where('tipo', 'P')->first();

        if (empty($pagina)){
            return redirect()->route('front::home')->with(["error" => "Página no encontrada"]);
        }

        $contenido = $this->obtenerSeccion($pagina, $id);

        $template = $request->ajax() ? "content.pagina_raw" : "content.pagina";
        return view($template, ["contenido" => $contenido, "menu" => null, "meta_descripcion" => "", "meta_keywords" => ""]);
    }

    public function mostrarBlogHome() {
        $posts = Blog::with(['categorias', 'autor'])
            ->activo()
            ->where('tipo', 'B')
            ->orderBy('created_at', 'desc')
            ->get();
        $categorias = CategoriaBlog::orderBy('nombre')->get();

        SEOMeta::setTitle('Blog');
        SEOMeta::setDescription('Escribímos algunos artículos para usted donde podrá encontrar últimas tendencias, consejos y tips sobre decoración.');

        return view('content.blog.home', ['posts' => $posts, 'categorias' => $categorias]);
    }

    public function mostrarBlogCategoria($id, $slug) {
        $categoria = CategoriaBlog::where('id', $id)
            ->where('slug', $slug)
            ->with([
                'posts' => function($q) {
                    $q->activo();
                },
                'posts.categorias', 
                'posts.autor'
            ])
            ->first();

        $categorias = CategoriaBlog::orderBy('nombre')->get();
        if (empty($categoria)) {
            return back()
                ->with(['error' => 'La categoría no existe']);
        }

        SEOMeta::setTitle($categoria->nombre);
        SEOMeta::setDescription($categoria->nombre);
        //SEOMeta::setCanonical('https://codecasts.com.br/lesson');

        OpenGraph::setDescription($categoria->nombre);
        OpenGraph::setTitle($categoria->nombre);
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'category');

        TwitterCard::setTitle($categoria->nombre);
        //TwitterCard::setSite('@LuizVinicius73');

        return view('content.blog.categoria', ['categoria' => $categoria, 'categorias' => $categorias]);
    }

    public function mostrarBlogArticulo($id, $slug) {
        $articulo = Blog::where('id', $id)
            ->activo()
            ->where('slug', $slug)
            ->where('tipo', 'B')
            ->with(['categorias', 'autor'])
            ->first();

        $categorias = CategoriaBlog::orderBy('nombre')->get();
        if (empty($articulo)) {
            return back()
                ->with(['error' => 'El artículo no existe']);
        }

        $desc = explode(" ", strip_tags($articulo->cuerpo));
        $desc = implode(" ", array_slice($desc, 0, 20));

        SEOMeta::setTitle($articulo->titulo);
        SEOMeta::setDescription($desc);
        //SEOMeta::setCanonical('https://codecasts.com.br/lesson');

        OpenGraph::setDescription($desc);
        OpenGraph::setTitle($articulo->titulo);
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'articles');

        TwitterCard::setTitle($articulo->titulo);
        //TwitterCard::setSite('@LuizVinicius73');

        $contenido = $this->obtenerSeccion($articulo, $id);

        return view('content.blog.articulo', ['articulo' => $articulo, 'contenido' => $contenido, 'categorias' => $categorias]);
    }
}
