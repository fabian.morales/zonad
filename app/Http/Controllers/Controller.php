<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        /*$catDecorativos = Categoria::where('categoria_id', 29)->with('hijos')->get();
        $catArq = Categoria::where('categoria_id', 30)->with('hijos')->get();
        \View::share('cat_decorativos', $catDecorativos);
        \View::share('cat_arq', $catArq);*/
        //\View::share('main_menu', $this->makeMenu());
    }

    private function makeMenu() {
        $menus = \App\Menu::where('mostrar','S')->whereNull('menu_id')->get();
        $ret = [];
        foreach ($menus as $menu) {
            $item = new \stdClass();
            $item->titulo = $menu->titulo;
            $item->link = '';
    
            if (!empty($menu->url)) {
                $item->link = $menu->url;
            }
            elseif (!empty($menu->slug)) {
                $item->link = url($menu->slug);
            }
            elseif ($menu->link_type == 'categoria') {
                $item->link = route('front::cat', ['id' => $menu->link_id, 'slug' => $menu->link->slug]);
                $item->hijos = [];
                foreach ($menu->link->hijos()->with('hijos')->get() as $hijo) {
                    $itemHijo = new \stdClass();
                    $itemHijo->titulo = $hijo->nombre;
                    $itemHijo->link = route('front::cat', ['id' => $hijo->id, 'slug' => $hijo->slug]);
    
                    if (sizeof($hijo->hijos)) {
                        $itemHijo->hijos = [];
                        foreach ($hijo->hijos as $nieto) {
                            $itemNieto = new \stdClass();
                            $itemNieto->titulo = $nieto->nombre;
                            $itemNieto->link = route('front::cat', ['id' => $nieto->id, 'slug' => $nieto->slug]);
    
                            $itemHijo->hijos[] = $itemNieto;
                        }
                    }
    
                    $item->hijos[] = $itemHijo;
                }
            }
            elseif ($menu->link_type == 'pagina') {
                $item->link = url($menu->link->slug);
            }
    
            $ret[] = $item;
        }
        
        return $ret;
    }
}
