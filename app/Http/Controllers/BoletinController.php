<?php

namespace App\Http\Controllers;

use App\Boletin;
use Illuminate\Http\Request;

class BoletinController extends Controller
{
    public function registrar(Request $request) {
        try {
            $correo = $request->get('correo');
            if (empty($correo)) {
                throw new \Exception("Debes ingresar tu correo");
            }

            if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception("El correo no es válido");
            }

            $control = $request->get('b_6bbb9b6f5827bd842d9640c82_05d85f18ef');
            if (!empty($control)) {
                throw new \Exception("No se puede procesar la petición");
            }

            $boletin = Boletin::where('correo', $correo)->first();
            if (empty($boletin)) {
                $boletin = new Boletin(['correo' => $correo]);
                $boletin->save();
            }

            return json_encode(['ok' => 1]);

        } 
        catch (\Exception $th) {
            return json_encode(['ok' => 0, 'msg' => $th->getMessage()]);
        }
        
    }
}
