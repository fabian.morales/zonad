<?php

namespace App\Http\Controllers;

use App\Categoria;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function mostrarProductos(Request $request, $id, $slug) {
        $categoria = Categoria::where('id', $id)
            ->where('slug', $slug)->with([
                'productos' => function($q) {
                    $q->activo()->orderBy('created_at', 'desc');
                },
                'productos.categorias', 
                'productos.imagenes'
            ])
            ->first();

        if (empty($categoria)) {
            return back()
                ->with(['error' => 'La categoría no existe']);
        }

        SEOMeta::setTitle($categoria->nombre);
        SEOMeta::setDescription($categoria->nombre);

        OpenGraph::setDescription($categoria->nombre);
        OpenGraph::setTitle($categoria->nombre);
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'category');

        TwitterCard::setTitle($categoria->nombre);

        return view('content.products.categoria', ['categoria' => $categoria]);
    }
}
