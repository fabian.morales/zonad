<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Atributo;
use Illuminate\Support\Facades\Validator;
use App\AtributoValor;

class AtributoController extends Controller
{
    public function mostrarIndex() {
        $atributos = Atributo::all();
        return view("admin.atributo.index", ["atributos" => $atributos]);
    }

    public function mostrarForm(Atributo $atributo) {
        if (empty($atributo)) {
            $atributo = new Atributo();
        }

        return view("admin.atributo.form", ["atributo" => $atributo]);
    }

    public function editar($id) {
        $atributo = Atributo::find($id);
        return $this->mostrarForm($atributo);
    }

    public function guardar(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'tipo' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $atributo = Atributo::findOrNew($id);

        $atributo->fill($request->all());
        if ($atributo->save()) {
            return redirect()
                ->route('admin::atrib::index')
                ->with(['mensaje' => 'Atributo guardado exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo guardar el atributo']);
        }
    }

    public function borrar($id) {
        $atributo = Atributo::find($id);
        if (empty($atributo) || $atributo == null) {
            return back()
                ->with(['error' => 'El atributo solicitado no existe']);
        }

        $atributo->valores()->delete();

        /**
         * TO-DO: borrado de los valores de atributos asociados a productos
         */
        //$atributo->valoresProductos()->detach();

        if ($atributo->delete()) {
            return redirect()
                ->route('admin::atrib::index')
                ->with(['mensaje' => 'Atributo borrado exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo borrar el atributo']);
        }
    }

    public function mostrarValor($id) {
        $valor = AtributoValor::find($id);
        $ret = ['ok' => 0];
        if (!empty($valor) && $valor != null) {
            $ret = ['ok' => 1, 'datos' => $valor];
        }

        return(json_encode($ret));
    }

    public function obtenerListaValores($id) {
        $valores = AtributoValor::where('atributo_id', $id)->get();
        $ret = "";
        if (sizeof($valores)) {
            $ret = $valores->toJson();
        }

        return $ret;
    }

    public function guardarValor(Request $request) {
        $id = $request->get('id');
        $valor = AtributoValor::findOrNew($id);
        $valor->fill($request->all());

        $ret = ['ok' => 0];

        if ($valor->save()) {
            $valores = AtributoValor::where('atributo_id', $valor->atributo_id)->get();
            $ret = [
                'ok' => 1,
                'datos' => $valores,
            ];
        }

        return json_encode($ret);
    }

    public function borrarValor($id) {
        $valor = AtributoValor::find($id);
        $ret = ['ok' => 0];

        if (empty($valor) || $valor == null) {
            return json_decode($ret);
        }

        $idAtributo = $valor->atributo_id;

        /**
         * TO-DO
         * borrar valores de atributo por producto
         */

        if ($valor->delete()) {
            $valores = AtributoValor::where('atributo_id', $idAtributo)->get();
            $ret = [
                'ok' => 1,
                'datos' => $valores
            ];
        }

        return json_encode($ret);
    }
}
