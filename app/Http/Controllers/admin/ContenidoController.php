<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contenido;
use Illuminate\Support\Facades\Validator;

class ContenidoController extends Controller
{
    public function mostrarIndex() {
        $paginas = Contenido::where('tipo', 'P')->get();
        return view("admin.contenido.index", ["paginas" => $paginas]);
    }

    public function mostrarForm(Contenido $pagina) {
        if (empty($pagina)) {
            $pagina = new Contenido();
        }

        return view("admin.contenido.form", ["pagina" => $pagina]);
    }

    public function editar($id) {
        $pagina = Contenido::find($id);
        return $this->mostrarForm($pagina);
    }

    public function guardar(Request $request){
        $validator = Validator::make($request->all(), [
            'titulo' => 'required',
            'slug' => 'required',
            'cuerpo' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $pagina = Contenido::findOrNew($id);

        $pagina->fill($request->all());
        $pagina->tipo = 'P';

        if (empty($pagina->inicio)) {
            $pagina->inicio = 'N';
        }

        if (empty($pagina->activo)) {
            $pagina->activo = 'N';
        }

        if (empty($pagina->peso)) {
            $pagina->peso = 0;
        }

        if ($pagina->save()) {
            if ($request->has('guardar_permanecer')) {
                return redirect()
                    ->route('admin::contenido::editar', ['id' => $pagina->id])
                    ->with(['mensaje' => 'Página guardada exitosamente']);
            }
            else{
                return redirect()
                    ->route('admin::contenido::index')
                    ->with(['mensaje' => 'Página guardada exitosamente']);
            }
        }
        else{
            return back()
                ->with(['error' => 'No se pudo guardar la página']);
        }
    }

    public function borrar($id) {
        $pagina = Contenido::find($id);
        if (empty($pagina) || $pagina == null) {
            return back()
                ->with(['error' => 'La página solicitada no existe']);
        }

        if ($pagina->delete()) {
            return redirect()
                ->route('admin::contenido::index')
                ->with(['mensaje' => 'Página borrada exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo borrar la página']);
        }
    }
}
