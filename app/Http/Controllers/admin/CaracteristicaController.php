<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Caracteristica;
use Illuminate\Support\Facades\Validator;

class CaracteristicaController extends Controller
{
    public function mostrarIndex() {
        $caracteristicas = Caracteristica::all();
        return view("admin.caracteristica.index", ["caracteristicas" => $caracteristicas]);
    }

    public function mostrarForm(Caracteristica $caracteristica) {
        if (empty($caracteristica)) {
            $caracteristica = new Caracteristica();
        }

        return view("admin.caracteristica.form", ["caracteristica" => $caracteristica]);
    }

    public function editar($id) {
        $caracteristica = Caracteristica::find($id);
        return $this->mostrarForm($caracteristica);
    }

    public function guardar(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $caracteristica = Caracteristica::findOrNew($id);

        $caracteristica->fill($request->all());
        if ($caracteristica->save()) {
            return redirect()
                ->route('admin::carac::index')
                ->with(['mensaje' => 'Característica guardada exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo guardar la característica']);
        }
    }

    public function borrar($id) {
        $caracteristica = Caracteristica::find($id);
        if (empty($caracteristica) || $caracteristica == null) {
            return back()
                ->with(['error' => 'La característica solicitada no existe']);
        }

        /**
         * TO-DO: borrado de los valores de características asociados a productos
         */
        //$caracteristica->valoresProductos()->detach();

        if ($caracteristica->delete()) {
            return redirect()
                ->route('admin::carac::index')
                ->with(['mensaje' => 'Característica borrada exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo borrar la característica']);
        }
    }
}
