<?php

namespace App\Http\Controllers\admin;

use App\Contacto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactoController extends Controller
{
    public function mostrarIndex() {
        $contactos = Contacto::all();
        return view("admin.contacto.index", ["contactos" => $contactos]);
    }

    public function descargarListado() {
        $contactos = Contacto::all();

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=contacto_" . date('Ymd_His_') . "_export_data.csv",
            "Pragma" => "no-cache",
            "Content-Encoding" => "UTF-8",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];
        $columnHeaders = ['ID', 'Nombre', 'Correo', 'Teléfono', 'Asunto', 'Mensaje', 'Aceptación de políticas', 'Fecha',];
        $callback = function() use ($contactos, $columnHeaders)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columnHeaders);
            foreach($contactos as $contacto) {        
                $datos = [$contacto->id, $contacto->nombre, $contacto->correo, $contacto->telefono, $contacto->asunto, $contacto->mensaje, $contacto->aceptacion_politicas, $contacto->created_at];
                array_walk($datos, function(&$value, $key){
                    $value = iconv('UTF-8', 'Windows-1252', $value);
                });
                fputcsv($file, $datos);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
