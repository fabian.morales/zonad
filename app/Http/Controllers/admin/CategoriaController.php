<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    public function mostrarIndex($id = "") {
        $categorias = [];

        if (!empty($id)) {
            $categoria = Categoria::find($id);
            if (empty($categoria) || $categoria == null) {
                return back()
                ->with(['error' => 'La categoría no existe']);
            }

            $categorias = Categoria::where('categoria_id', $id)->orderBy('nombre')->get();
        }
        else{
            $categorias = Categoria::whereNull('categoria_id')->get();
        }
        
        return view("admin.categoria.index", ["categorias" => $categorias]);
    }

    public function mostrarForm(Categoria $categoria) {
        if (empty($categoria)) {
            $categoria = new Categoria();
        }

        $categorias = Categoria::where('id', '!=', $categoria->id)->orderBy('nombre')->get();

        return view("admin.categoria.form", ["categoria" => $categoria, "categorias" => $categorias]);
    }

    public function editar($id) {
        $categoria = Categoria::find($id);
        return $this->mostrarForm($categoria);
    }

    public function guardar(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'slug' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $categoria = Categoria::findOrNew($id);

        $categoria->fill($request->all());
        if ($categoria->save()) {
            return redirect()
                ->route('admin::categ::index')
                ->with(['mensaje' => 'Categoría guardada exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo guardar la categoría']);
        }
    }

    public function borrar($id) {
        $categoria = Categoria::find($id);
        if (empty($categoria) || $categoria == null) {
            return back()
                ->with(['error' => 'La categoría solicitado no existe']);
        }

        if ($categoria->hijos()->count() > 0) {
            return back()
                ->with(['error' => 'La categoría no se puede borrar porque contiene categorías hijas']);
        }

        /**
         * TO-DO: borrado de las categorias asociadas a productos
         */
        //$categoria->productos()->detach();

        if ($categoria->delete()) {
            return redirect()
                ->route('admin::categ::index')
                ->with(['mensaje' => 'Categoría borrada exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo borrar la categoría']);
        }
    }
}
