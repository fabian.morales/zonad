<?php

namespace App\Http\Controllers\admin;

use App\Galeria;
use App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Request;

class GaleriaController extends Controller {

    public function mostrarIndex() {
        $galerias = \App\Galeria::orderBy("id")->get();
        
        return view('admin.galeria.index', array("galerias" => $galerias));
    }

    public function mostrarForm(\App\Galeria $galeria) {
        if (empty($galeria)) {
            $galeria = new \App\Galeria();
        }

        return view("admin.galeria.form", array("galeria" => $galeria));
    }

    public function editar($id) {
        $galeria = \App\Galeria::with("fotos")->where("id", $id)->first();
        if (empty($galeria)) {
            return back()
            ->with(['error' => 'No se pudo encontrar la galería']);
        }

        return $this->mostrarForm($galeria);
    }
    
    public function guardar(Request $request) {
        $id = $request->get("id");

        $galeria = \App\Galeria::find($id);
        if (empty($galeria)) {
            $galeria = new \App\Galeria();
        }

        $galeria->fill($request->all());
        $car = ["á", "é", "í", "ó", "ú", "ï", "ü", " ", ",", "/", "?", "¿", "!", "¡", "-"];
        $rep = ["a", "e", "i", "o", "u", "i", "u", "_", "_", "", "", "", "", "", "_"];

        $galeria->llave = str_replace($car, $rep, strtolower($galeria->titulo));
        
        if (empty($galeria->titulo)){
            return back()->with(["error" => "Debe ingresar el nombre de la galería"]);
        }

        if ($galeria->save()) {
            if ($request->has('guardar_permanecer')) {
                return redirect()
                    ->route('admin::gal::editar', ["id" => $galeria->id])
                    ->with(["mensaje" => "Galería guardada exitosamente"]);
            }
            else{
                return redirect()
                    ->route('admin::gal::index', ["id" => $galeria->id])
                    ->with(["mensaje" => "Galería guardada exitosamente"]);
            }
        } 
        else {
            return back()->with(["error" => "No se pudo guardar la galería"]);
        }
    }
    
    function borrar($id){
        $galeria = Galeria::with("fotos")->where("id", $id)->first();
        
        if (empty($galeria)){
            return back()->with(["error" => "Galería no encontrada"]);
        }
        
        $fotos = $galeria->fotos();
        
        if (!$galeria->fotos()->delete()){
            return back()->with(["error" => "No se pudo borrar las fotos de la galería"]);
        }
        
        if ($galeria->delete()){
            foreach($fotos as $f){
                $path = public_path('storage/imagenes/galeria/'.$f->id_galeria.'/'.$f->id.'.jpg');
                @unlink($path);
            }
            
            @rmdir(public_path('storage/imagenes/galeria/'.$galeria->id));
            
            return redirect()->route('admin::gal::index')->with(["mensaje" => "Galería borrada exitosamente"]);
        }
        else{
            return back()->with(["error" => "No se pudo borrar la galería"]);
        }
    }
    
    public function editarFoto($id){
        $foto = \App\Foto::find($id);
        if (empty($foto)) {
            $foto = new \App\Foto();
        }
        
        return $foto->toJson();
    }

    public function guardarFoto(Request $request) {
        $id = $request->get("id");

        $foto = \App\Foto::find($id);
        if (empty($foto)) {
            $foto = new \App\Foto();
        }

        $foto->fill($request->all());
        $foto->descripcion = $foto->descripcion ?? '';
        if (empty($foto->archivo)) {
            $foto->archivo = "";
        }
        
        if (empty($foto->titulo)){
            return json_encode(["ok" => 0, "error" => "Debe ingresar el nombre de la foto"]);
        }
        
        $imagen = $request->file("imagen");
    
        if (!$foto->id && (empty($imagen) || !$imagen->isValid())) {
            return json_encode(["ok" => 0, "error" => "Debe ingresar la imagen"]);
        }

        if ($foto->save()) {
            
            if (!empty($imagen) && $imagen->isValid()){
                $path = public_path('storage/imagenes/galeria/'.$foto->galeria_id.'/');

                if (!is_dir($path)) {
                    $partesPath = ['storage', 'imagenes', 'galeria', $foto->galeria_id];
                    $subpath = public_path() . '/';
                    foreach ($partesPath as $p) {
                        $subpath .= $p . '/';

                        if (!is_dir($subpath)) {
                            mkdir($subpath);
                        }
                    }
                }

                $filename = $foto->id . "." . $imagen->extension(); 
                $imagen->move($path, $filename);
                $foto->archivo = $filename;
                $foto->save();
            }

            return json_encode(["ok" => 1, "mensaje" => "Foto guardada exitosamente", "fotos" => $foto->galeria()->first()->fotos()->get()]);
        } 
        else {
            return json_encode(["ok" => 0, "error" => "No se pudo guardar la foto"]);
        }
    }
    
    function borrarFoto($id){
        $foto = \App\Foto::find($id);
        
        if (empty($foto)){
            return json_encode(["ok" => 0, "error" => "Foto no encontrada"]);
        }
        
        $path = public_path('storage/imagenes/galeria/'.$foto->galeria_id.'/'.$foto->archivo);
        
        if ($foto->delete()){
            @unlink($path);
            return json_encode(["ok" => 1, "mensaje" => "Foto eliminada exitosamente", "fotos" => $foto->galeria()->first()->fotos()->get()]);
        }
        else{
            return json_encode(["ok" => 0, "error" => "No se pudo eliminar la foto"]);
        }
    }
}
