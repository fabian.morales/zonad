<?php

namespace App\Http\Controllers\admin;

use App\Blog;
use App\CategoriaBlog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageHelper;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    public function mostrarIndex() {
        $posts = Blog::where('tipo', 'B')->get();
        return view("admin.blog.index", ["posts" => $posts]);
    }

    public function mostrarForm(Blog $post, $categorias = null) {
        if (empty($post)) {
            $post = new Blog();
        }

        if (empty($categorias)) {
            $categorias = CategoriaBlog::all();
        }

        return view("admin.blog.form", ["post" => $post, "categorias" => $categorias]);
    }

    public function editar($id) {
        $post = Blog::find($id);
        $categorias = CategoriaBlog::with(['posts' => function($q) use ($id) {
            $q->where('blog_id', $id);
        }])->get();
        return $this->mostrarForm($post, $categorias);
    }

    public function guardar(Request $request){
        $validator = Validator::make($request->all(), [
            'titulo' => 'required',
            'slug' => 'required',
            'cuerpo' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $post = Blog::findOrNew($id);

        $post->fill($request->all());
        $post->tipo = 'B';
        $post->inicio = 'N';
        $post->peso = 0;

        if (empty($post->activo)) {
            $post->activo = 'N';
        }

        if ($post->save()) {
            $post->categorias()->sync($request->get('categorias'));
            if ($request->hasFile('imagen')) {
                $file = $request->file('imagen');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'imagenes', 'blog', $post->id];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $post->imagen = $filename;
                $post->save();

                ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('inner'), 770, 450);
                ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('featured'), 670, 450);
                ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('thumbnail'), 370, 220);
                ImageHelper::makeImage($post->getOriginalImagePath(), $post->getImagePath('tiny'), 70, 70);
            }

            if ($request->has('guardar_permanecer')) {
                return redirect()
                    ->route('admin::blog::editar', ['id' => $post->id])
                    ->with(['mensaje' => 'Post guardado exitosamente']);
            }
            else{
                return redirect()
                    ->route('admin::blog::index')
                    ->with(['mensaje' => 'Post guardado exitosamente']);
            }
        }
        else{
            return back()
                ->with(['error' => 'No se pudo guardar el post']);
        }
    }

    public function borrar($id) {
        $post = Blog::find($id);
        if (empty($post) || $post == null) {
            return back()
                ->with(['error' => 'El post solicitado no existe']);
        }

        if ($post->delete()) {
            return redirect()
                ->route('admin::blog::index')
                ->with(['mensaje' => 'Post borrado exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo borrar el página']);
        }
    }
}
