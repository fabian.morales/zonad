<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Atributo;
use App\AtributoValor;
use App\Caracteristica;
use App\CaracteristicaProducto;
use Illuminate\Support\Facades\Validator;
use App\Producto;
use App\Categoria;
use Illuminate\Support\Collection;
use App\ImagenProducto;
use Image;

class ProductoController extends Controller
{
    public function mostrarIndex() {
        $productos = Producto::with('imagenes')->get();
        return view("admin.producto.index", ["productos" => $productos]);
    }

    public function mostrarForm(Producto $producto, Collection $categorias = null, Collection $caracteristicas = null) {
        if (empty($producto)) {
            $producto = new Producto();
        }

        if ($categorias == null) {
            $categorias = Categoria::with('hijos.hijos.hijos')->get();
        }

        if ($caracteristicas == null) {
            $caracteristicas = Caracteristica::all();
        }

        $atributos = Atributo::with('valores')->get();
        $principal = null;

        return view("admin.producto.form", compact("producto", "categorias", "caracteristicas", "atributos", "principal"));
    }

    public function editar($id) {
        $producto = Producto::where('id', $id)->with(['imagenes'])->first();
        $categorias = Categoria::with(['productos' => function($q) use ($id) {
            $q->where('producto_id', $id);
        }])->get();
        $principal = $categorias->first(function($valor) {
            return !empty($valor->productos) && sizeof($valor->productos) && $valor->productos[0]->pivot->principal == 1;
        });
        $caracteristicas = Caracteristica::with(['valores' => function($q) use($id) {
            $q->where('producto_id', $id);
        }])->get();
        $atributos = Atributo::with('valores')->get();

        return view("admin.producto.form", compact("producto", "categorias", "caracteristicas", "atributos", "principal"));
    }

    public function guardar(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'slug' => 'required',
            'codigo' => 'required',
            'descripcion' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $producto = Producto::findOrNew($id);

        $producto->fill($request->all());
        $producto->descripcion_oferta = $producto->descripcion_oferta ?? '';

        if (empty($producto->destacado) || !$request->has('destacado')) {
            $producto->destacado = 'N';
        }

        if (empty($producto->activo) || !$request->has('activo')) {
            $producto->activo = 'N';
        }

        if ($producto->save()) {
            $idPrincipal = $request->get("categoria_principal");
            $categorias = array_flip($request->get('categorias'));
            array_walk($categorias, function(&$valor, $indice) use($idPrincipal) {
                $valor = [
                    'principal' => $indice == $idPrincipal ? 1 : null,
                ];
            });

            $producto->categorias()->sync($categorias);
            foreach ($request->get('caracteristicas') as $index => $carac) {
                if (empty($carac)) {
                    CaracteristicaProducto::where('producto_id', $producto->id)
                        ->where('caracteristica_id', $index)
                        ->delete();
                        
                    continue;
                }

                $caracteristica = CaracteristicaProducto::where('producto_id', $producto->id)
                    ->where('caracteristica_id', $index)
                    ->firstOrNew(['producto_id' => $producto->id, 'caracteristica_id' => $index]);

                $caracteristica->valor = $carac;
                $caracteristica->save();
            }

            if ($request->has('archivo_oferta')) {
                $file = $request->file('archivo_oferta');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'imagenes', 'productos', $id, 'oferta'];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $producto->archivo_oferta = $filename;
                $producto->save();
            }

            if ($request->has('guardar_permanecer')) {
                return redirect()
                    ->route('admin::prod::editar', ['id' => $producto->id])
                    ->with(['mensaje' => 'Producto guardado exitosamente']);
            }
            else{
                return redirect()
                    ->route('admin::prod::index')
                    ->with(['mensaje' => 'Producto guardado exitosamente']);
            }
        }
        else{
            return back()
                ->with(['error' => 'No se pudo guardar el producto']);
        }
    }

    public function borrar($id) {
        $producto = Producto::find($id);
        if (empty($producto) || $producto == null) {
            return back()
                ->with(['error' => 'El producto solicitado no existe']);
        }

        $producto->categorias()->detach();
        foreach ($producto->imagenes ?? '' as $imagen) {
            if (is_file($imagen->getPath())) {
                @unlink($imagen->getPath());
                @unlink($imagen->getThumbPath());
            }

            $imagen->delete();
        }
        //$producto->imagenes()->delete();

        $producto->atributos()->detach();
        $producto->caracteristicas()->delete();

        /**
         * TO-DO: borrado de las ofertas asociadas a productos
         */
        //$producto->ofertas()->delete();

        /**
         * TO-DO: borrado de los tags asociados a productos
         */
        //$producto->tags()->delete();

        if ($producto->delete()) {
            return redirect()
                ->route('admin::prod::index')
                ->with(['mensaje' => 'Producto borrado exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo borrar el producto']);
        }
    }

    public function guardarImagen(Request $request, $id) {
        $imagen = new ImagenProducto();
        $imagen->producto_id = $id;
        $file = $request->file('file');
        $filename = uniqid() . '.' . $file->extension();

        $path = ['storage', 'imagenes', 'productos', $id];
        $dir = public_path();
        foreach ($path as $p) {
            $dir .= '/' . $p;
            if (!is_dir($dir)) {
                mkdir($dir);
            }
        }

        $dirThumb = $dir .'/thumb';
        if (!is_dir($dirThumb)) {
            mkdir($dirThumb);
        }

        //$resp = $file->storeAs('storage/imagenes/productos/' . $id, $filename);
        $file->move($dir, $filename);
        $imagen->archivo = $filename;
        $imagen->save();

        $img = Image::make($dir . '/' . $filename);
        $img->resizeCanvas(263, 316, 'center', true, 'ffffff')
        ->fit(263, 316, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->save($dirThumb . '/' . $filename);

        $producto = Producto::find($id);
        return $producto->imagenes()->get()->toJson();
    }

    public function borrarImagenes(Request $request, $id) {
        $ids = $request->get('borrar_img');
        foreach ($ids as $i) {
            $imagen = ImagenProducto::find($i);
            if (!empty($imagen) && $imagen != null && $imagen->producto_id == $id) {
                if (is_file($imagen->getPath())) {
                    @unlink($imagen->getPath());
                    @unlink($imagen->getThumbPath());
                }

                $imagen->delete();
            }
        }

        $producto = Producto::find($id);
        return $producto->imagenes()->get()->toJson();
    }

    public function obtenerListaAtributos($id) {
        $producto = Producto::where('id', $id)->with(['atributosValor.atributo'])->first();
        $ret = "";
        if (!empty($producto) && !empty($producto->atributosValor)) {
            $ret = $producto->atributosValor->toJson();
        }

        return $ret;
    }

    public function guardarAtributo(Request $request) {
        $idProducto = $request->get('id_producto');
        $idAtributo = $request->get('id_atributo');
        $idValor = $request->get('id_valor');

        $producto = Producto::find($idProducto);
        $valor = AtributoValor::where('id', $idValor)->with('producto')->whereHas('producto', function($q) use ($idProducto, $idValor) {
            $q->where('atributo_valor_producto.producto_id', $idProducto);
        })->first();

        //return json_encode($valor);

        $ret = ['ok' => 0];
        if (empty($valor)) {
            $producto->atributosValor()->attach([$idValor => ['atributo_id' => $idAtributo, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]]);
            $producto = Producto::where('id', $idProducto)->with('atributosValor.atributo')->first();
            $valores = $producto->atributosValor;
            $ret = [
                'ok' => 1,
                'datos' => $valores,
            ];
        }

        return json_encode($ret);
    }

    public function borrarValor($id) {
        $ret = ['ok' => 0];

        $valor = AtributoValor::with('producto')->whereHas('producto', function($q) use ($id) {
            $q->where('atributo_valor_producto.id', $id);
        })->get();

        if (!empty($valor) && $valor->producto()->detach([$valor->producto[0]->id])) {
            if ($valor->delete()) {
                $producto = Producto::where('id', $valor->producto[0]->id)->with('atributosValor.atributo')->first();
                $valores = $producto->atributosValor();
                $ret = [
                    'ok' => 1,
                    'datos' => $valores
                ];
            }
        }

        return json_encode($ret);
    }
}
