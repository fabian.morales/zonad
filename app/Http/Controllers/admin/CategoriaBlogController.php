<?php

namespace App\Http\Controllers\admin;

use App\CategoriaBlog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriaBlogController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'categoria_blog';
    }

    public function mostrarIndex() {
        $categorias = CategoriaBlog::all();
        return view('admin.categoria_blog.index', compact('categorias'));
    }

    public function mostrarForm(CategoriaBlog $categoria) {
        if (empty($categoria)) {
            $categoria = new Categoria();
        }

        return view("admin.categoria_blog.form", ["categoria" => $categoria]);
    }

    public function editar($id) {
        $categoria = CategoriaBlog::find($id);
        return $this->mostrarForm($categoria);
    }

    public function guardar(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'slug' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $categoria = CategoriaBlog::findOrNew($id);

        $categoria->fill($request->all());
        if ($categoria->save()) {
            return redirect()
                ->route('admin::categ_blog::index')
                ->with(['mensaje' => 'Categoría guardada exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo guardar la categoría']);
        }
    }

    public function borrar($id) {
        $categoria = CategoriaBlog::find($id);
        if (empty($categoria) || $categoria == null) {
            return back()
                ->with(['error' => 'La categoría solicitado no existe']);
        }

        /**
         * TO-DO: borrado de las categorias asociadas a productos
         */
        //$categoria->productos()->detach();

        if ($categoria->delete()) {
            return redirect()
                ->route('admin::categ_blog::index')
                ->with(['mensaje' => 'Categoría borrada exitosamente']);
        }
        else{
            return back()
                ->with(['error' => 'No se pudo borrar la categoría']);
        }
    }
}
