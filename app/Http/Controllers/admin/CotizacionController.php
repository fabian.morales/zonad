<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cotizacion;

class CotizacionController extends Controller
{
    public function mostrarIndex() {
        $cotizaciones = Cotizacion::orderBy('created_at', 'desc')->get();
        return view("admin.cotizacion.index", ["cotizaciones" => $cotizaciones]);
    }

    public function mostrar($id) {
        $cotizacion = Cotizacion::with(['producto.imagenes', 'valores.atributoValor.atributo'])
            ->where('id', $id)
            ->first();

        if (empty($cotizacion)) {
            return back()
                ->with(['error' => 'La cotización no existe']);
        }

        return view("admin.cotizacion.form", ["cotizacion" => $cotizacion]);
    }
}
