<?php

namespace App\Http\Controllers\admin;

use App\Boletin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BoletinController extends Controller
{
    public function mostrarIndex() {
        $boletines = Boletin::all();
        return view("admin.boletin.index", ["boletines" => $boletines]);
    }

    public function descargarListado() {
        $boletines = Boletin::all();

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=boletines_" . date('Ymd_His_') . "_export_data.csv",
            "Pragma" => "no-cache",
            "Content-Encoding" => "UTF-8",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];
        $columnHeaders = ['ID', 'Correo', 'Fecha'];
        $callback = function() use ($boletines, $columnHeaders)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columnHeaders);
            foreach($boletines as $boletin) {        
                $datos = [$boletin->id, $boletin->correo, $boletin->created_at];
                fputcsv($file, $datos);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
