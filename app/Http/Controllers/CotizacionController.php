<?php

namespace App\Http\Controllers;

use App\Cotizacion;
use App\CotizacionAtributoValor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CotizacionController extends Controller
{
    public function recibirCotizacion(Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'ciudad' => 'required',
            'correo' => 'required',
            'celular' => 'required',
            'mensaje' => 'required',
            'acepta_terminos' => 'required',
            'producto_id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(["ok" => 0, "msg" => "Datos incompletos"]);
        }

        $cotizacion = new Cotizacion();
        $cotizacion->fill($request->all());
        $cotizacion->estado = 'N';

        if (empty($cotizacion->whatsapp)) {
            $cotizacion->whatsapp = 'N';
        }

        if (empty($cotizacion->acepta_terminos)) {
            $cotizacion->acepta_terminos = 'N';
        }

        if ($cotizacion->save()) {
            $valores = $request->get('valor_producto');
            if (!empty($valores)) {
                $valoresAtributo = [];
                foreach ($valores as $valor) {
                    $valorAtributo = new CotizacionAtributoValor();
                    $valorAtributo->cotizacion_id = $cotizacion->id;
                    $valorAtributo->atributo_valor_id = $valor;
                    $valoresAtributo[] = $valorAtributo;
                }

                $cotizacion->valores()->saveMany($valoresAtributo);
            }

            $cotizacionEml = Cotizacion::with(['producto', 'valores.atributoValor.atributo'])->where('id', $cotizacion->id)->first();
            $mailsTo = explode(",", env('MAIL_TO'));
            \Mail::to($mailsTo)->send(new \App\Mail\CotizacionNuevaAdmin($cotizacionEml));
            \Mail::to($cotizacionEml->correo)->send(new \App\Mail\CotizacionNuevaUsuario($cotizacionEml));

            return json_encode(["ok" => 1]);
        }
        else{
            return json_encode(["ok" => 0, "msg" => "No se pudo guardar la cotización"]);
        }
    }
}
