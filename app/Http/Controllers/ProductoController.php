<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;

class ProductoController extends Controller
{
    public function mostrarProducto(Request $request, $id_cat, $slug_cat, $id, $slug) {
        $producto = Producto::where('id', $id)
            ->activo()
            ->where('slug', $slug)
            ->whereHas('categorias', function (Builder $query) use($id_cat, $slug_cat) {
                $query->where('categoria.id', $id_cat)->where('categoria.slug', $slug_cat);
            })
            ->with(['categorias', 'imagenes', 'caracteristicas.caracteristica', 'atributos.valoresProducto' => function($q) use ($id) {
                $q->where('producto_id', $id);
            }])
            ->first();

        if (empty($producto)) {
            return back()
                ->with(['error' => 'El producto existe']);
        }

        $desc = explode(" ", strip_tags($producto->descripcion));
        $desc = implode(" ", array_slice($desc, 0, 20));

        SEOMeta::setTitle($producto->nombre);
        SEOMeta::setDescription($desc);
        //SEOMeta::setCanonical('https://codecasts.com.br/lesson');

        OpenGraph::setDescription($desc);
        OpenGraph::setTitle($producto->nombre);
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'products');

        TwitterCard::setTitle($producto->nombre);
        //TwitterCard::setSite('@LuizVinicius73');

        return view('content.products.producto', ['producto' => $producto]);
    }
}
