<?php

namespace App\Http\Controllers;

use App\Contacto;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ContactoController extends Controller {

    public function mostrarContacto()
    {
        SEOMeta::setTitle('Contacto');
        SEOMeta::setDescription('Déjanos saber sus dudas e inquietudes. Nuestro equipo se encuentra a su disposición para atenderle tan pronto como sea posible.');

        return view('content.pages.contacto');
    }

    public function registrar(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'correo' => 'required',
                'asunto' => 'required',
                'mensaje' => 'required',
                'telefono' => 'required',
                'aceptacion_politicas' => 'required',
                //'g-recaptcha-response' => 'required'
            ]);

            if ($validator->fails()) {
                throw new \Exception("Debes completar tus datos");
            }

            // $remote = \Illuminate\Support\Facades\Request::ip();
            // $resCap = $request->get("g-recaptcha-response");

            // $validaCap = $this->validarCaptcha($resCap, $remote);
            // if ($validaCap != true){
            //     throw new \Exception('El captcha no es válido');
            // }

            $correo = $request->get('correo');
            if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception("El correo no es válido");
            }

            $control = $request->get('b_6bbb9b6f5827bd842d9640c82_05d85f18ef');
            if (!empty($control)) {
                throw new \Exception("No se puede procesar la petición");
            }

            $contacto = new Contacto();
            $contacto->fill($request->all());
            $contacto->save();

            $mailsTo = explode(",", env('MAIL_TO'));
            \Mail::to($mailsTo)->send(new \App\Mail\ContactoNuevoAdmin($contacto));
            \Mail::to($contacto->correo)->send(new \App\Mail\ContactoNuevoUsuario($contacto));

            return json_encode(['ok' => 1]);

        }
        catch (\Exception $th) {
            return json_encode(['ok' => 0, 'msg' => $th->getMessage()]);
        }

    }

    private function validarCaptcha($response, $remote){
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $fields = [
            'secret' => env('GOOGLE_RC_SECRET'),
            'response' => $response,
            'remoteip' => $remote
        ];

        $fields_string = '';
        foreach($fields as $key=>$value) {
            $fields_string .= $key.'='.$value.'&';
        }

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);

        $retorno = false;
        if (isset($result->success) && $result->success){
            $retorno = true;
        }

        return $retorno;

    }
}
