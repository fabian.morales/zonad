<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Producto;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        SEOMeta::setTitle('Inicio');
        SEOMeta::setDescription('En Adiela De Lombana contamos con un portafolio integral de acabados arquitectónicos y decorativos, abarcando el mercado institucional, comercial, industrial y residencial.');

        $productos = Producto::where('destacado', 'S')
            ->activo()
            ->with(['categorias', 'imagenes'])
            ->orderBy('created_at', 'desc')
            ->get();

        $posts = Blog::with(['categorias', 'autor'])
            ->activo()
            ->where('tipo', 'B')
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get();

        return view('content.pages.home', ['productos' => $productos, 'posts' => $posts]);
    }

    public function mostrarNosotros()
    {
        SEOMeta::setTitle('Nosotros');
        SEOMeta::setDescription('Somos Un centro de diseño que ofrece variedad de productos, diseño exclusivo, asesoría profesional, y un punto de encuentro para diseñadores, arquitectos y clientes.');

        return view('content.pages.nosotros');
    }

    public function mostrarTYC()
    {
        SEOMeta::setTitle('Política de privacidad');
        SEOMeta::setDescription('ADIELA DE LOMBANA S.A. en cumplimiento de lo estipulado en la Ley 1581 de 2012 “Por la cual se dictan disposiciones generales para la protección de datos personales”.');

        return view('content.pages.terminos_condiciones');
    }

}
