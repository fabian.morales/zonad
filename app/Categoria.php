<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categoria';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'slug', 'categoria_id'
    ];

    public function padre() {
        return $this->belongsTo(Categoria::class);
    }

    public function hijos() {
        return $this->hasMany(Categoria::class);
    }

    public function productos() {
        return $this
            ->belongsToMany(Producto::class, 'producto_categoria')
            ->withPivot('producto_id', 'principal');
    }

    public function links() {
        return $this->morphMany(Menu::class, 'link');
    }
}
