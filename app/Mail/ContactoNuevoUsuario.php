<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactoNuevoUsuario extends Mailable
{
    use Queueable, SerializesModels;

    private $contacto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Contacto $contacto)
    {
        $this->contacto = $contacto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->bcc(env('MAIL_SOPORTE'))
            ->subject('Solicitud de contacto recibida')
            ->view('emails.contacto_nuevo_usuario')
            ->with(['contacto' => $this->contacto]);
    }
}
