<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CotizacionNuevaUsuario extends Mailable
{
    use Queueable, SerializesModels;

    private $cotizacion;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Cotizacion $cotizacion)
    {
        $this->cotizacion = $cotizacion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->bcc(env('MAIL_SOPORTE'))
            ->subject('Solicitud de cotización recibida')
            ->view('emails.cotizacion_nueva_usuario')
            ->with(['cotizacion' => $this->cotizacion]);
    }
}
