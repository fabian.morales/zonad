<?php 

namespace App\Helpers;
use Image;

class ImageHelper{
    public static function makeImage($original, $dest, $width, $height, $resizeCanvas = true) {
        @unlink($dest);
        $img = Image::make($original);
        $prop = $width / $height;

        $finalWidth = $width;
        $finalHeight = $height;

        if ($img->height() > $finalHeight) {
            $finalHeight = $img->height();
            $finalWidth = $finalHeight * $prop;
        }

        if ($img->width() > $finalWidth) {
            $finalWidth = $img->width();
            $finalHeight = $finalWidth / $prop;
        }

        if ($resizeCanvas) {
            $img->resizeCanvas($width, $height, 'center', true, 'ffffff')
                ->fit($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save($dest);
        }
        else {
            $img->fit(round($finalWidth), round($finalHeight), function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save($dest);
        }
    }
}
