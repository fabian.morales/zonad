<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtributoValor extends Model
{
    protected $table = 'atributo_valor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'etiqueta', 'valor', 'atributo_id'
    ];

    public function atributo() {
        return $this->belongsTo(Atributo::class);
    }

    public function producto() {
        return $this->belongsToMany(Producto::class, 'atributo_valor_producto')->withPivot(['producto_id', 'atributo_id']);
    }
}
