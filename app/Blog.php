<?php

namespace App;

use App\Traits\ImageTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;
    use ImageTrait;
    
    protected $table = 'contenido';
    protected $fillable = [
        'slug', 'titulo', 'tipo', 'cuerpo', 'peso', 'inicio', 'activo'
    ];

    protected $folderName = 'blog';
    protected $idField = "id";
    protected $fileField = "imagen";

    public function scopeActivo($query) {
        return $query->where('activo', 'S');
    }

    public function links() {
        return $this->morphMany(Menu::class, 'link');
    }

    public function categorias() {
        return $this->belongsToMany(CategoriaBlog::class, 'contenido_categoria');
    }

    public function autor() {
        return $this->belongsTo(User::class);
    }

    public function getImagenPath() {
        $ret = public_path('storage/imagenes/blog');
        if (!empty($this->id) && (int)$this->id > 0) {
            $ret .= '/' . $this->id . '/' . $this->imagen;
        }

        return $ret;
    }

    public function getImagenUrl() {
        $ret = 'storage/imagenes/blog';
        if (!empty($this->id) && (int)$this->id > 0) {
            $ret .= '/' . $this->id . '/' . $this->imagen;
        }

        return $ret;
    }
}
