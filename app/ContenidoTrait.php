<?php

namespace App;

trait ContenidoTrait{
        
    public function obtenerSeccionesInicio(){
        $secciones = Contenido::where("tipo", "S")->where("inicio", "S")->orderBy("peso")->get();
        $ret = [];
        foreach ($secciones as $s){
            $ret[] = ['html' => $this->obtenerSeccion($s), 'movil' => $s->movil];
        }
        
        return $ret;
    }
    
    public function procesarVariables($contenido){
        $galerias = [];
        $patron_gal = "/\\\$galeria\\|(.[^\\s\\!]*)/";
        
        if (strpos($contenido, "{!! \$galeria|") !== false){
            $salida = [];
            preg_match_all($patron_gal, $contenido, $salida);
            
            if (count($salida) >= 2){
                foreach($salida[1] as $s){
                    $galerias["galeria_".$s] = '';
                }
                
                $gals = Galeria::with("fotos")->whereIn("llave", $salida[1])->get();
                foreach ($gals as $g){
                    $tmpFotos = view('content.galeria', ["galeria" => $g])->render();
                    $galerias["galeria_".$g->llave] = $tmpFotos;
                }
            }
        }
              
        $vars = [];
        
        $cuerpo = $contenido;        
        if (sizeof($galerias)){
            $vars = array_merge($vars, $galerias);
            $cuerpo = preg_replace($patron_gal, "\$galeria_$1", $cuerpo);    
        }

        $html = \Blade::compileString($cuerpo);
        return $this->renderSeccion($html, $vars);
    }
    
    public function obtenerSeccion($seccion, $idSeccion=null){
                
        if (empty($seccion)){
            $seccion = Contenido::find($idSeccion);
        }
        
        return $this->procesarVariables($seccion->cuerpo);
    }
    
    function renderSeccion($__php, $__data)
    {
        $obLevel = ob_get_level();
        ob_start();
        extract($__data, EXTR_SKIP);
        try {
            eval('?' . '>' . $__php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }
        $ret = ob_get_clean();
        return $ret;
    }
}