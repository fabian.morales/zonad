<!doctype html>
@inject('widget', 'App\Services\WidgetsService')
<html class="no-js" lang="en">

<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MR6PJ6Q');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    {!! SEOMeta::generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('front/img/favicon.png') }}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    <!-- All CSS Files -->
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    <!-- Icon Font -->
    <link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/pe-icon-7-stroke.css') }}">
    <!-- Plugins css file -->
    <link rel="stylesheet" href="{{ asset('front/css/plugins.css') }}">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{{ asset('front/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{ asset('front/css/responsive.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/brands.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/v4-shims.css">

    <!-- Modernizr JS -->
    <script src="{{ asset('front/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    @section('css_body')
    @show
    <!-- custom BODY code-->
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script data-cfasync="false" >(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script data-cfasync="false"  type="text/javascript">
    window.fbAsyncInit = function(){
        FB.init({
            appId: '2515761085369883',
            status: true,
            cookie: true,
            xfbml: true,
            version: 'v2.8'
        });
    };
    </script>
    <!-- end custom BODY code-->
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MR6PJ6Q"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div class="caja_flotante hidden-sm hidden-xs">
    <ul>
        <li><a href="https://wa.me/573203509866" target="_blank"><img src="{{ asset('img/whatsapp.png') }}" alt ="WhatsApp" /></a></li>
        <li><a href="https://m.me/zonad.adl" target="_blank"><img src="{{ asset('img/messenger.png') }}" alt ="Messenger" /></a></li>
        <li><a href="/contacto"><img src="{{ asset('img/mail.png') }}" alt ="Contacto" /></a></li>
    </ul>
</div>
<!-- Body main wrapper start -->
<div class="wrapper">

<!-- START HEADER SECTION -->
<header class="header-section section sticker header-transparent">
    <div class="container-fluid upper-header">
        <div class="row">
            <div class="col-md-6 hidden-xs hidden-sm">
                <small>&iexcl;Hablemos! <a class="cta" href="https://wa.me/573203509866"><i class="fa fa-whatsapp"></i> 320 3509866</a></small>
                <small><i class="fa fa-envelope"></i> email: <a class="cta" href="mailto:contacto@zonad-adl.co">contacto@zonad-adl.co</a></small>
            </div>
            <div class="col-md-6 text-right hidden-xs hidden-sm">
                <div class="header-social fix">
                    <medium>
                        <span>&iexcl;Síguenos!</span>
                            <a class="cta" href="https://www.facebook.com/zonad.adl/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a class="cta" href="https://www.instagram.com/zonad.adl/" target="_blank"><i class="fa fa-instagram"></i></a>
                    </medium>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center hidden-md hidden-lg block-xs block-sm">
                <small>&iexcl;Hablemos! <a class="cta" href="https://wa.me/573203509866"><i class="fa fa-whatsapp"></i> 320 3509866</a></small>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <!-- logo -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="header-logo">
                    <a href="{{ url('/') }}"><img id="site-logo" src="{{ asset('/front/img/logo-adiela.png') }}" alt="Adiela De Lombana" /></a>
                </div>
            </div>
            <!-- primary-menu -->
            <div class="col-md-9 col-sm-6 col-xs-12">
                <nav class="main-menu text-center">
                    <ul>
                        @php
                        $main_menu = $widget->makeMenu()
                        @endphp
                        @foreach($main_menu as $item)
                        <li>
                            <a href="{{ $item->link }}">{{ $item->titulo }}</a>
                            @if(!empty($item->hijos))
                            <ul class="mega-menu">
                                @foreach($item->hijos as $hijo)
                                <li><a href="{{ $hijo->link }}">{{ $hijo->titulo }}</a>
                                    @if (!empty($hijo->hijos))
                                    <ul>
                                        @foreach($hijo->hijos as $nieto)
                                        <li><a href="{{ $nieto->link }}">{{ $nieto->titulo }}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="mobile-menu"></div>
            </div>
        </div>
    </div>
</header>
<!-- END HEADER SECTION -->

@yield('content')

<!-- SERVICE SECTION START -->
<div class="service-section section pt-70 pb-40 hidden-xs">
    <div class="container">
        <div class="row">
            <!-- Single Service -->
            <div class="single-service col-md-3 col-sm-6 col-xs-6 mb-30">
                <div class="service-icon">
                    <i class="fa fa-couch"></i>
                </div>
                <div class="service-title">
                    <h3>INNOVACIÓN Y <br />DISEÑO</h3>
                </div>
            </div>
            <!-- Single Service -->
            <div class="single-service col-md-3 col-sm-6 col-xs-6 mb-30">
                <div class="service-icon">
                    <i class="fas fa-thumbs-up"></i>
                </div>
                <div class="service-title">
                    <h3>EXCELENTE ASESORÍA <br />Y SERVICIO</h3>
                </div>
            </div>
            <!-- Single Service -->
            <div class="single-service col-md-3 col-sm-6 col-xs-6 mb-30">
                <div class="service-icon">
                    <i class="fas fa-check-circle"></i>
                </div>
                <div class="service-title">
                    <h3>PRODUCTOS DE ALTA <br />CALIDAD</h3>
                </div>
            </div>
            <!-- Single Service -->
            <div class="single-service col-md-3 col-sm-6 col-xs-6 mb-30">
                <div class="service-icon">
                    <i class="fas fa-handshake"></i>
                </div>
                <div class="service-title">
                    <h3>SEGURIDAD Y<br /> CONFIANZA</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SERVICE SECTION END -->

<!-- FOOTER TOP SECTION START -->
<div class="footer-top-section section pb-60 pt-100">
	<div class="container">
		<div class="row">
            <!-- Footer Widget -->
			<div class="footer-widget col-md-3 col-sm-6 col-xs-12 mb-40">
				<h4 class="widget-title">Contacto Sur</h4>
				<p>
                    <i class="pe-7s-map-marker"></i> Calle 16 No 103 - 44<br /> Cali, Colombia<br />
				    <i class="pe-7s-call"></i> (2) 333 14 23 - (2) 332 73 19<br />
				    <i class="fa fa-whatsapp"></i> 320 3509866<br />
                    <i class="fa fa-envelope"></i> contacto@zonad-adl.co
                </p>

				<!-- Footer Social -->
				<div class="footer-social fix">
					<a href="https://www.facebook.com/zonad.adl/" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
					<a href="https://www.instagram.com/zonad.adl/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
				</div>
			</div>
			<!-- Footer Widget -->
			<div class="footer-widget col-md-3 col-sm-6 col-xs-12 mb-40">
				<h4 class="widget-title">Contacto Norte</h4>
				<p>
                    <i class="pe-7s-map-marker"></i> Av 6 AN # 20N - 05 Santa M&oacute;nica<br /> Cali, Colombia<br />
				    <i class="pe-7s-call"></i> (2) 668 68 00 - (2) 668 68 66<br />
				    <i class="fa fa-whatsapp"></i> 320 3509866<br />
                    <i class="fa fa-envelope"></i> contacto@zonad-adl.co
                </p>
			</div>
			<!-- Footer Widget -->
			<div class="footer-widget col-md-3 col-sm-6 col-xs-12 mb-40 hidden-xs">
                <h4 class="widget-title">Temas de interés</h4>
                @php
                $catBlog = $widget->getBlogCategories()
                @endphp
				<div class="tag-cloud fix">
                    @foreach($catBlog as $cat)
                    <a href="{{ route('front::blog::cat', ['id' => $cat->id, 'slug' => $cat->slug]) }}">{{ $cat->nombre }}</a>
                    @endforeach
				</div>
			</div>
			<!-- Footer Widget -->
			<div class="footer-widget col-md-3 col-sm-6 col-xs-12 mb-40">
				<h4 class="widget-title">Suscribirse</h4>
				<p>Entérate de todas las novedades y ofertas que tenemos para ti. </p>
				<form action="{{ route('front::boletin') }}" method="post" id="form_suscribir" name="form_suscribir" class="sunscribe-form validate" target="_blank" novalidate>
                    @csrf
                    <div id="mc_embed_signup_scroll">
                        <label for="mce-EMAIL" class="hidden">Subscribe to our mailing list</label>
                        <input type="email" value="" name="correo" class="email" id="email_boletin" placeholder="Correo electrónico" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Suscríbete" name="subscribe" id="btnSuscribir" class="button"></div>
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>
<!-- FOOTER TOP SECTION END -->

<!-- FOOTER BOTTOM SECTION START -->
<div class="footer-bottom-section section pb-20 pt-20">
	<div class="container">
		<div class="row">
            <!-- Copyright -->
			<div class="copyright text-center col-sm-3 col-xs-12">
				&nbsp;
			</div>
			<div class="copyright text-center col-sm-6 col-xs-12">
				<p>Copyright {{ date('Y') }} &copy; Adiela D`Lombana - Todos los derechos reservados</p>
			</div>
			<!-- Payment Method -->
			<!--<div class="payment-method text-right col-sm-3 col-xs-12">
				<img src="{{ asset('front/img/payment/1.png') }}" alt="payment" />
				<img src="{{ asset('front/img/payment/2.png') }}" alt="payment" />
				<img src="{{ asset('front/img/payment/3.png') }}" alt="payment" />
				<img src="{{ asset('front/img/payment/4.png') }}" alt="payment" />
			</div>-->
		</div>
	</div>
</div>
<!-- FOOTER BOTTOM SECTION END -->



</div>
<!-- Body main wrapper end -->


<!-- Placed JS at the end of the document so the pages load faster -->

<!-- jQuery latest version -->
<script src="{{ asset('front/js/vendor/jquery-3.1.1.min.js') }}"></script>
<!-- Bootstrap js -->
<script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
<!-- Plugins js -->
<script src="{{ asset('front/js/plugins.js') }}"></script>
<!-- Ajax Mail js -->
<script src="{{ asset('front/js/ajax-mail.js') }}"></script>

@section('js_body')
@show

<!-- Main js -->
<script src="{{ asset('front/js/main.js') }}"></script>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5946690.js"></script>
<!-- End of HubSpot Embed Code -->

</body>

</html>
