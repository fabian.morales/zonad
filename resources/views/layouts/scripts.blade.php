    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <!-- Custom scripts for all pages-->
    <script>
        (function(w, $) {
            w.frontier_admin = {
                base_url: "{{ url('/') }}"
            };
            /*$(document).ready(function() {

            });*/
        })(window, jQuery);
    </script>
    <script src="{{ asset('js/sb-admin-2.js') }}"></script>