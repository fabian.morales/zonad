@extends('layouts.front')

@section('content')
<div class='content paginas'>
    <div class="container">
        <div class="row">
            {!! $contenido !!}
        </div>
    </div>
</div>
@endsection