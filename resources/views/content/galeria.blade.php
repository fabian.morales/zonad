<div class="container_full">
    <div class="row no-gutter">
        @if(!empty($galeria->fotos))
        
        @foreach($galeria->fotos as $f)
        <div class="col-sm-6 col-md-3">
            <div class="item foto">
                <a href="{{ asset('storage/imagenes/galeria/' . $f->galeria_id . '/' . $f->archivo) }}" data-featherlight>
                    <img src="{{ asset('storage/imagenes/galeria/' . $f->galeria_id . '/' . $f->archivo) }}" title="{{ $f->descripcion }}" alt="{{ $f->titulo }}" />
                </a>
            </div>
        </div>
        @endforeach
        
        @endif
    </div>
</div>