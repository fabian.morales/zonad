@extends('layouts.front')

@section('content')
<div class="content">
    <div class="category">
        <div class="title">
            <h1>Blog</h1>
            <p>últimas tendencias y noticias</p>
        </div>
    </div>
</div>

<!-- PAGE SECTION START -->
<div class="page-section section">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-xs-12">
                <div class="row two-column">
                    @include('content.blog.lista_posts', ['posts' => $posts])
                    @php
                    /*<div class="page-pagination text-center col-xs-12 fix mb-40">
                        <ul>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>*/
                    @endphp
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-12">
                @php
                /*<div class="single-sidebar mb-40">
                    <form action="#" class="sidebar-search">
                        <input type="text" placeholder="Search here...">
                        <button class="submit"><i class="pe-7s-search"></i></button>
                    </form>
                </div>
                
                <div class="single-sidebar mb-40">
                    <h3 class="sidebar-title">Popular Post</h3>
                    <div class="post-sidebar">
                        <div class="post fix">
                            <a href="#" class="image"><img src="img/blog/sp-1.jpg" alt=""></a>
                            <div class="content fix">
                                <h5 class="title"><a href="#">The history of furniture</a></h5>
                                <span class="author">by <a href="#">Julie Bates</a></span>
                            </div>
                        </div>
                        <div class="post fix">
                            <a href="#" class="image"><img src="img/blog/sp-2.jpg" alt=""></a>
                            <div class="content fix">
                                <h5 class="title"><a href="#">The history of furniture</a></h5>
                                <span class="author">by <a href="#">Julie Bates</a></span>
                            </div>
                        </div>
                        <div class="post fix">
                            <a href="#" class="image"><img src="img/blog/sp-3.jpg" alt=""></a>
                            <div class="content fix">
                                <h5 class="title"><a href="#">The history of furniture</a></h5>
                                <span class="author">by <a href="#">Julie Bates</a></span>
                            </div>
                        </div>
                    </div>
                </div>*/
                @endphp
                
                @include('content.blog.lista_categorias', ['categorias' => $categorias])

                @php
                /*<div class="single-sidebar mb-40">
                    <h3 class="sidebar-title">Archive</h3>
                    <ul class="archive-sidebar">
                        <li><a href="#">May 2017</a></li>
                        <li><a href="#">April 2017</a></li>
                        <li><a href="#">March 2017</a></li>
                        <li><a href="#">February 2017</a></li>
                        <li><a href="#">January 2017</a></li>
                        <li><a href="#">December 2016</a></li>
                        <li><a href="#">November 2016</a></li>
                    </ul>
                </div>
               
                <div class="single-sidebar mb-40">
                    <h3 class="sidebar-title">Popular Tags</h3>
                    <div class="tag-cloud">
                        <a href="#">Chairs</a>
                        <a href="#">Tables</a>
                        <a href="#">Sofas</a>
                        <a href="#">Lights</a>
                        <a href="#">Lamps</a>
                        <a href="#">curtains</a>
                        <a href="#">cabinets</a>
                    </div>
                </div>*/
                @endphp
            </div>
        </div>
    </div>
</div>
<!-- PAGE SECTION END -->
@endsection