<div class="single-sidebar mb-40">
    <h3 class="sidebar-title">Categorias</h3>
    <ul class="category-sidebar">
        @foreach($categorias as $cat)
        <li>
            <a href="{{ route('front::blog::cat', ['id' => $cat->id, 'slug' => $cat->slug]) }}">{{ $cat->nombre }}</a>
        </li>
        @endforeach
    </ul>
</div>
