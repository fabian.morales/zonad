@extends('layouts.front')

@section('content')
<div class="content">
    <div class="category">
        <div class="title">
            <h1>{{ $categoria->nombre }}</h1>
        </div>
    </div>
</div>

<!-- PAGE SECTION START -->
<div class="page-section section">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-xs-12">
                <div class="row two-column">
                    @include('content.blog.lista_posts', ['posts' => $categoria->posts])
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-12">
                @include('content.blog.lista_categorias', ['categorias' => $categorias])
            </div>
        </div>
    </div>
</div>
<!-- PAGE SECTION END -->
@endsection