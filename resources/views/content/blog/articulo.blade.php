@extends('layouts.front')

@section('content')
<!-- PAGE SECTION START -->
<div class="content paginas page-section section pt-100 pb-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-xs-12">
                <!-- Single Blog Post -->
                <div class="single-blog-post">
                    <div class="blog-img">
                    <img class="img-responsive" src="{{ asset($articulo->getImagenUrl()) }}" alt="{{ $articulo->titulo }}" />
                    </div>
                    <div class="blog-info">
                        <h3 class="title">{{ $articulo->titulo }}.</h3>
                        <div class="blog-meta">
                            @if(!empty($articulo->autor))
                            <span><a href="#"><i class="fa fa-user"></i> {{ $articulo->autor->nombre }}</a></span>
                            @endif
                            <!--span><a href="#"><i class="fa fa-tags"></i> Uncategories</a></span-->
                        </div>
                        {!! $contenido !!}
                        <br />
                        <ul class="list">
                            <li><a href="#" data-share="facebook"><i class="fa fa-facebook-f"></i> Facebook</a></li>
                            <li><a href="#" data-share="twitter" data-msg="{{ $articulo->titulo }}"><i class="fa fa-twitter"></i> Twitter</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-12">
                @include('content.blog.lista_categorias', ['categorias' => $categorias])
            </div>
        </div>
    </div>
</div>
<!-- PAGE SECTION END --> 
@endsection