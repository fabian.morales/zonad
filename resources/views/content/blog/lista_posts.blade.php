@foreach($posts as $post)
<!-- blog-item start -->
@php
setlocale(LC_ALL, 'esp');
$fecha = strtotime($post->created_at);
if (!isset($hasMd) || empty($hasMd)) {
    $hasMd = 0;
}
$desc = explode(" ", strip_tags($post->cuerpo));
$desc = array_slice($desc, 0, 50);

@endphp
<div class="@if($hasMd) col-md-4 @endif col-sm-6 col-xs-12 mb-40">
    <div class="blog-item">
        <a class="image" href="{{ route('front::blog::art', ['id' => $post->id, 'slug' => $post->slug]) }}"><img src="{{ asset($post->getImagenUrl()) }}" alt="{{ $post->titulo }}"></a>
        <div class="blog-dsc">
            <span class="date">{{ strftime('%d %b %G ', $fecha) }}</span>
            <h4 class="title"><a href="{{ route('front::blog::art', ['id' => $post->id, 'slug' => $post->slug]) }}">{{ $post->titulo }}.</a></h4>
            <p> {!! implode(" ", $desc) !!}...</p>
            @if(!empty($articulo->autor))
            <span class="author">by <a href="#">{{ $articulo->autor->nombre }}</a></span>
            @endif          
        </div>
    </div>
</div>
<!-- blog-item end -->
@endforeach