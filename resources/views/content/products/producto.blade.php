@extends('layouts.front')

@section('content')
<div class="content">
    <div class="product">
        <div class="title">
            <h1>@if (!empty($producto->categorias)) {{ $producto->categorias[0]->nombre }} / @endif {{ $producto->nombre }}</h1>
        </div>
    </div>

    <div class="container product-info">
        <div class="row">
            <div class="col-md-3">
                @if (sizeof($producto->imagenes))
                <div class="tab-content mb-10">
                    @foreach($producto->imagenes as $imagen)
                    <div class="pro-large-img tab-pane @if($loop->first) active @endif" id="pro-large-img-{{ $imagen->id }}">
                        <a href="{{ asset($imagen->getUrl()) }}" data-featherlight>
                            <img src="{{ asset($imagen->getThumbUrl()) }}" alt="{{ $producto->nombre }}" />
                        </a>
                    </div>
                    @endforeach
                <!--img class="img-responsive" src="{{ asset($producto->imagenes[0]->getUrl()) }}" alt="{{ $producto->nombre }}"/-->
                </div>
                <!-- Nav tabs -->
                <div class="pro-thumb-img-slider">
                    @foreach($producto->imagenes as $imagen)
                    <div><a href="#pro-large-img-{{ $imagen->id }}" data-toggle="tab"><img src="{{ asset($imagen->getThumbUrl()) }}" alt="" /></a></div>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="col-md-9">
                <h2>{{ $producto->nombre }}</h2>
                <!--<p>Disponible:
                    @if ($producto->en_stock == 'S')
                    <i class="fas fa-check green"></i> En stock
                    @else
                    <i class="fas fa-times red"></i> Sin stock
                    @endif
                </p>-->
                <p> {!! $producto->descripcion !!}</p>

                @if (!empty($producto->atributos))
                @foreach ($producto->atributos as $atributo)
                <ul class="list">
                    @foreach ($atributo->valoresProducto as $valor)
                    <li>
                        <label class="color-selector" style="background-color: {{ $valor->valor }}" title="{{ $valor->etiqueta }}">
                            <input type="radio" rel="valores_producto" name="valor_producto[{{ $atributo->id }}]" value="{{ $valor->id }}" id="valor_producto_{{ $valor->id }}" @if ($loop->first) checked="checked" @endif />
                            <span class="color-checked"><i class="fas fa-check"></i></span>
                        </label>
                    </li>
                    @endforeach
                </ul>
                <br />
                @endforeach
                @endif

                <ul class="list">
                    <li><a href="#"><i class="fas fa-envelope"></i> Enviar por email</a></li>
                    <!--<li><a href="#"><i class="fas fa-heart"></i> Favorito</a></li>
                    <li><a href="#"><i class="fas fa-sync"></i> Comparar</a></li>-->
                </ul>
                <br />
                <div class="row">
                    <div class="col-md-3">
                        <input type="button" value="SOLICITAR COTIZACIÓN" id="btnCotizar" class="btn btn-black" />
                    </div>
                </div>
                <br />
                <ul class="list">
                    <li><a href="#" data-share="facebook"><i class="fa fa-facebook-f"></i> Facebook</a></li>
                    <li><a href="#" data-share="twitter" data-msg="{{ $producto->nombre }}: {{ strip_tags($producto->descripcion) }}"><i class="fa fa-twitter"></i> Twitter</a></li>
                </ul>
            </div>
        </div>

        <div class="row nav-product-info">
            <div class="col-xs-12">
                <ul class="pro-info-tab-list section">
                    @php
                    /*<li class="active"><a href="#desc-tab" data-toggle="tab">Descripci&oacute;n</a></li>*/
                    @endphp
                    <li class="active"><a href="#ficha-tab" data-toggle="tab">Ficha Técnica</a></li>
                    <li><a href="#obs-tab" data-toggle="tab">Observaciones</a></li>
                </ul>
            </div>

            <div class="tab-content col-xs-12">
                @php
                /*<div class="pro-info-tab tab-pane active" id="desc-tab">
                    {!! $producto->descripcion !!}
                </div>*/
                @endphp
                <div class="pro-info-tab tab-pane active" id="ficha-tab">
                    @if(!empty($producto->caracteristicas))
                    <table class="table-data-sheet table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>Caracter&iacute;stica</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($producto->caracteristicas as $valor)
                            <tr>
                                <td>{{ $valor->caracteristica->nombre }}</td>
                                <td>{{ $valor->valor }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h3>Este producto a&uacute;n no tiene ficha t&eacute;cnica</h3>
                    @endif
                </div>
                <div class="pro-info-tab tab-pane" id="obs-tab">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="cotizacion_modal" tabindex="-1" role="dialog" aria-labelledby="cotizacion_modal_label1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content contact-form">
            <form id="cotizacion" action="{{ route('front::cot') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="cotizacion_modal_label" style="float: left; font-size: 2rem; color: #888;">Solicita una cotización de este producto</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="producto_id" name="producto_id" value="{{ $producto->id }}" />
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="nombre" class="col-form-label-sm">Nombre</label-->
                            <input type="text" id="nombre" name="nombre" placeholder="Nombre" class="form-control" />
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="ciudad" class="col-form-label-sm">Ciudad</label-->
                            <input type="text" id="ciudad" name="ciudad" placeholder="Ciudad" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="correo" class="col-form-label-sm">Correo</label-->
                            <input type="email" id="correo" name="correo" placeholder="Correo" class="form-control" />
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="celular" class="col-form-label-sm">Celular</label-->
                            <input type="text" id="celular" name="celular" placeholder="Celular" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <!--label for="mensaje" class="col-form-label-sm">Mensaje</label-->
                            <textarea id="mensaje" name="mensaje" placeholder="Mensaje" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label for="whatsapp" class="col-form-label-sm"><input type="checkbox" name="whatsapp" id="whatsapp" value="S" /> &iquest;Usas WhatsApp?</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label for="acepta_terminos" class="col-form-label-sm"><input type="checkbox" name="acepta_terminos" id="acepta_terminos" value="S" /> Aceptar <a href="{{ route('front::tyc') }}" target="_blank">t&eacute;rminos y condiciones</a></label>
                        </div>
                    </div>
                        
                    <div class="modal-footer">
                        <a id="btnEnviar" class="btn btn-primary btn-cotizacion btn-icon-split float-left" href="">
                            <span class="icon text-white-50">
                                <i class="fas fa-paper-plane"></i>
                            </span>
                            <span class="text">Enviar</a>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('css_body')
<link rel="stylesheet" href="{{ asset('front/js/featherlight/featherlight.min.css') }}" />
@stop

@section('js_body')
<script src="{{ asset('front/js/featherlight/featherlight.min.js') }}"></script>
@stop