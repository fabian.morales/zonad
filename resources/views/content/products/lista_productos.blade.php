<div class="row">
    @foreach($productos as $producto)
    <!-- product-item start -->
    <div class="col-md-3 col-sm-4 col-xs-6 col-xs-wide mb-40">
        <div class="product-item text-center">
            <div class="product-img">
                @php
                $img = asset('/front/img/product.png');
                if (sizeof($producto->imagenes)) {
                    $img = asset($producto->imagenes[0]->getThumbUrl());
                }

                $urlProducto = $producto->obtenerLink();

                @endphp
                <a class="image" href="{{ $urlProducto }}"><img src="{{ $img }}" alt="{{ $producto->nombre }}"/></a>
                <!--a href="#" class="add-to-cart">add to cart</a-->
                <div class="action-btn fix">
                    <a href="#" title="Favoritos"><i class="pe-7s-like"></i></a>
                    <a href="#" data-toggle="modal"  data-target="#productModal" title="Ver detalle"><i class="pe-7s-look"></i></a>
                    <a href="#" title="Comparar"><i class="pe-7s-repeat"></i></a>
                </div>
            </div>
            <div class="product-info">
                <h5 class="title"><a href="{{ $urlProducto }}">{{ $producto->nombre }}</a></h5>
                <!--span class="price"><span class="new">$169.00</span></span-->
            </div>
        </div>
    </div>
    @endforeach
    <!-- product-item end -->
</div>