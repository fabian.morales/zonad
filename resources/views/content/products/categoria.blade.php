@extends('layouts.front')

@section('content')
<div class="content">
    <div class="category">
        <div class="title">
            <h1>{{ $categoria->nombre }}</h1>
            <p>Nuestros productos</p>
        </div>
    </div>
    @if (sizeof($categoria->productos))
    <div class="product-section section pb-60">
        <div class="container">
        @include('content.products.lista_productos', ['productos' => $categoria->productos])
        </div>
    </div>
    @endif
</div>
@endsection