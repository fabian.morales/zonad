@extends('layouts.front')

@section('content')
<div class="content">
    <div class="category">
        <div class="title">
            <h1>Quienes somos</h1>
            <p>Conoce acerca de nosotros</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p>Con una trayectoria de m&aacute;s de 30 a&ntilde;os, Adiela de Lombana S.A es la empresa pionera y l&iacute;der a
            nivel nacional en la comercializaci&oacute;n, distribuci&oacute;n, instalaci&oacute;n y fabricaci&oacute;n de
            productos para la construcci&oacute;n en seco y con dos sedes especializadas en Decoraci&oacute;n.</p>
        <p>Su unidad de negocio, Adiela de Lombana Decoraci&oacute;n se renueva manteniendo sus valores de experiencia,
                solidez y excelente servicio al cliente, evolucionando a&nbsp;<strong>Zona D Adiela
                D&rsquo;Lombana</strong>, Un centro de dise&ntilde;o que ofrece variedad de productos, dise&ntilde;o
                exclusivo, asesor&iacute;a profesional, una oferta integral y un punto de encuentro para dise&ntilde;adores,
                arquitectos y clientes.</p>
        <p><strong>Zona D Adiela D&rsquo;Lombana&nbsp;</strong>cuenta con un portafolio amplio e integral de acabados
                arquitect&oacute;nicos y decorativos, abarcando el mercado institucional, comercial, industrial y
                residencial.</p>
        <br />
        <h3 class="text-center text-uppercase">Nuestros Aliados</h3>
        <hr>
        <br />
        @inject('widget', 'App\Services\WidgetsService')
        {!! $widget->renderGallery('nuestros_aliados') !!}
    </div>
</div>
@endsection
