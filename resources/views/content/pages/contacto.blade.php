@extends('layouts.front')

@section('content')
<!-- PAGE SECTION START -->
<div class="content">
    <div class="category">
        <div class="title">
            <h1>Contacto</h1>
            <p>Pongase en contacto con nosotros</p>
        </div>
    </div>
</div>

<div class="page-section section pb-60">
    <div class="container">
        <div class="row">
            <div class="contact-info col-md-4 col-xs-12 mb-40">
                <h3>Sala Sur</h3>
                <p><i class="fa fa-map-marker"></i><span>Calle 16 No 103- 44, Ciudad Jardín<br />Cali, Colombia</span></p>
                <p><i class="fa fa-phone"></i><span>333 14 23 - 332 73 19</span></p>
                <p><i class="fa fa-whatsapp"></i><a href="https://wa.me/573203509866" target="_blank">320 3509866</a></p>
                <p><i class="fab fa-waze"></i><a href="https://www.waze.com/ul?ll=3.3644304%2C-76.5317276&navigate=yes&zoom=17" target="_blank">C&oacute;mo llegar</a></p>

                <hr />

                <h3>Sala Norte</h3>
                <p><i class="fa fa-map-marker"></i><span>Av 6 AN # 20N - 05 Santa M&oacute;nica<br />Cali, Colombia</span></p>
                <p><i class="fa fa-phone"></i><span>668 68 00 - 668 68 66</span></p>
                <p><i class="fa fa-whatsapp"></i><a href="https://wa.me/573203509866" target="_blank">320 3509866</a></p>
                <p><i class="fab fa-waze"></i><a href="https://www.waze.com/ul?ll=3.462097%2C-76.531344&navigate=yes&zoom=17" target="_blank">C&oacute;mo llegar</a></p>

                <div class="contact-social">
                    <a href="https://www.facebook.com/zonad.adl/" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/zonad.adl/" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <div class="contact-form col-md-8 col-xs-12 mb-40">
                <h3>Solicita m&aacute;s informaci&oacute;n. &iexcl;Escr&iacute;benos!</h3>
                <hr>
                <form id="contact-form" action="{{ route('front::contacto::post') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 mb-20">
                            <label for="nombre">Nombre *</label>
                            <input id="nombre" name="nombre" type="text" required>
                        </div>
                        <div class="col-sm-6 col-xs-12 mb-20">
                            <label for="correo">Correo *</label>
                            <input id="correo" name="correo" type="email" required>
                        </div>
                        <div class="col-sm-6 col-xs-12 mb-20">
                            <label for="telefono">Tel&eacute;fono *</label>
                            <input id="telefono" name="telefono" type="text" required>
                        </div>
                        <div class="col-sm-6 col-xs-12 mb-20">
                            <label for="asunto">Asunto *</label>
                            <input id="asunto" name="asunto" type="text" required>
                        </div>
                        <div class="col-xs-12 mb-20">
                            <label for="mensaje">Mensaje *</label>
                            <textarea name="mensaje" id="mensaje"></textarea>
                        </div>
                        <div class="col-xs-12 mb-20">
                            <label>
                                <input type="checkbox" name="aceptacion_politicas" id="aceptacion_politicas" required value="S" />
                                <span>Declaro que he leído y acepto la <a href="/docs/Politicas-tratamiento-ADL.pdf">política de tratamiento de datos personales.</a></span>
                            </label>
                            
                        </div>
                        <div class="col-xs-12 mb-20">
                        <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RC_SITE') }}"></div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                            <input type="submit" id="btnEnviarContacto" value="Enviar Mensaje">
                        </div>
                    </div>
                </form>
                <p class="form-messege"></p>
            </div>
            <div class="col-xs-12 mt-40">
                <div id="contact-map">
                    <iframe id="gmap_canvas" class="gmap_canvas" src="https://maps.google.com/maps?q=3.3644304,-76.5317276&t=&z=19&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display: none"><img id="logo-map" src="{{ asset('img/logo-mini.png') }}" alt="main logo" /></div>
<!-- PAGE SECTION END -->
@endsection

@section('js_body')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDZQ5FMqihwCw2XnjHiHEKHwdEFMsuQa4"></script>
<!--script src="{{ asset('front/js/map.js') }}"></script-->
@stop
