@extends('layouts.front')

@section('content')
<!-- START SLIDER SECTION -->
<div class="home-slider-section section">
    <!-- Home Slider -->
    <div id="home-slider" class="slides">
{{--        <a href="https://wa.me/573203509866" target="_blank">--}}
{{--            <img src="{{ asset('img/Banner-dia-de-madres.jpg') }}" alt="" title="#slider-caption-0"  />--}}
{{--        </a>--}}
{{--        <a href="https://wa.me/573203509866" target="_blank">--}}
{{--            <img src="{{ asset('img/adl-decoracion-banner-principal-diciembre.jpg') }}" alt="" title="#slider-caption-00"  />--}}
{{--        </a>--}}
{{--        <img src="{{ asset('img/banner-papeles-colgadura-abril-2022-min.png') }}" alt="" title="#slider-caption-01"  />--}}
        <img src="{{ asset('img/banner-1-min.jpg') }}" alt="" title="#slider-caption-1"  />
        <img src="{{ asset('img/banner-2-min.png') }}" alt="" title="#slider-caption-2"  />
        <img src="{{ asset('img/banner-3-min.png') }}" alt="" title="#slider-caption-3"  />
{{--        <img src="{{ asset('img/adiela-de-lombana-decoracion-asoleadoras.jpg') }}" alt="" title="#slider-caption-4"  />--}}
    </div>
    <!-- Caption 0 -->
    <div id="slider-caption-0" class="nivo-html-caption">
        {{--        <div class="container">--}}
        <div class="row">
            <div class="hero-slider-content col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 col-xs-12">
{{--                <h1 class="wow fadeInUp" data-wow-duration="1s">Silla Colgante Pagoda</h1>--}}
{{--                <p class="wow fadeInUp" data-wow-duration="1s">Disfruta leyendo un buen libro en un rincón especial de tu terraza con nuestra línea de muebles para balcones</p>--}}
            <!--a href="{{ url('/') }}" class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">Buy now</a-->
            </div>
        </div>
        {{--        </div>--}}
    </div>
    <!-- Caption 01 -->
    <div id="slider-caption-01" class="nivo-html-caption">
        {{--        <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="hero-slider-content col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 col-xs-12">--}}
{{--                <h1 class="wow fadeInUp" data-wow-duration="1s">Cerraduras, manijas y accesorios</h1>--}}
{{--                <p class="wow fadeInUp" data-wow-duration="1s">Alternativas en diseño y seguridad como herramientas que personalizan tus espacios.</p>--}}
{{--            <!--a href="{{ url('/') }}" class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">Buy now</a-->--}}
{{--            </div>--}}
{{--        </div>--}}
        {{--        </div>--}}
    </div>
    <!-- Caption 1 -->
    <div id="slider-caption-1" class="nivo-html-caption">
{{--        <div class="container">--}}
            <div class="row">
                <div class="hero-slider-content col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 col-xs-12">
                    <h1 class="wow fadeInUp" data-wow-duration="1s">Saint Elena</h1>
                    <p class="wow fadeInUp" data-wow-duration="1s">Tapetes que aportan diseño y color a tus espacios</p>
                    <!--a href="{{ url('/') }}" class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">Buy now</a-->
                </div>
            </div>
{{--        </div>--}}
    </div>
    <!-- Caption 2 -->
    <div id="slider-caption-2" class="nivo-html-caption">
{{--        <div class="container">--}}
            <div class="row">
                <div class="hero-slider-content col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 col-xs-12">
                    <h1 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">Luminaria</h1>
                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">Luminarias que brindan elegancia y diseño a tus ambientes interiores</p>
                    <!--a href="{{ url('/') }}" class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">Buy now</a-->
                </div>
            </div>
{{--        </div>--}}
    </div>
    <!-- Caption 3 -->
    <div id="slider-caption-3" class="nivo-html-caption">
{{--        <div class="container">--}}
            <div class="row">
                <div class="hero-slider-content col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 col-xs-12">
                    <h1 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">Set de alcoba</h1>
                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">Simplicidad y sofisticación, la combinación perfecta</p>
                <!--a href="{{ url('/') }}" class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">Buy now</a-->
                </div>
            </div>
{{--        </div>--}}
    </div>
    <!-- Caption 3 -->
    <div id="slider-caption-4" class="nivo-html-caption">
{{--        <div class="container">--}}
            <div class="row">
                <div class="hero-slider-content col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 col-xs-12">
                    <h1 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">Asoleadora Venecia</h1>
                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">El complemento perfecto para un exterior impactante.</p>
                <!--a href="{{ url('/') }}" class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">Buy now</a-->
                </div>
            </div>
        </div>
{{--    </div>--}}
</div>
<!-- END SLIDER SECTION -->

<!-- BANNER-SECTION START -->
<div class="banner-section section pt-100 pb-70 hidden-sm hidden-xs">
    <div class="container">
        <div class="row">
            <!-- Banner Item Start -->
            <div class="col-sm-6 col-xs-12 mb-30">
                <div class="single-banner">
                    <a href="{{ url('productos/29-acabados-decorativos') }}">
                        <h1 class="productos">Decorativos</h1>
                        <img src="{{ asset('img/acabados-decorativos.jpg') }}" alt="">
                    </a>
                </div>
            </div>
            <!-- Banner Item End -->
            <!-- Banner Item Start -->
            <div class="col-sm-6 col-xs-12 mb-30">
                <div class="single-banner">
                    <a href="{{ url('productos/30-acabados-arquitectonicos') }}">
                        <h1 class="productos">Arquitectónicos</h1>
                        <img src="{{ asset('img/acabados-arquitectonicos.jpg') }}" alt="">
                    </a>
                </div>
            </div>
            <!-- Banner Item End -->
            <div class="col-sm-12 col-xs-12 mb-30">
                <div class="single-banner">
                    <a href="https://cortinas.adieladelombana.com/" target="_blank">
                        <h1 class="productos av dealer-site">Cortinas y Persianas</h1>
                        <img src="{{ asset('img/banner-dealer-site.jpg') }}" alt="">
                    </a>
                </div>
            </div>
            <!-- Banner Item End -->
        </div>
    </div>
</div>
<!-- BANNER-SECTION END -->

@if (sizeof($productos))
<!-- PRODUCT SECTION START -->
<div class="product-section section pt-60 pb-60">
    <div class="container">
        <div class="row">
            <div class="section-title text-center col-xs-12 mb-70">
                <h4>Nuestros productos</h4>
                <h2>Destacados</h2>
            </div>
        </div>
        @include('content.products.lista_productos', ['productos' => $productos])
    </div>
</div>
<!-- PRODUCT SECTION END -->
@endif
@php
/*<!-- TESTIMONIAL SECTION START -->
<div class="testimonial-section section pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="single-testimonial text-center">
                    <h1 class="fabrica">Fabrica tu mueble</h1>
                    <p>Somos aliados de tus ideas, renueva tus espacios con tus espacios con tus propios diseños. ¡Recibe asesoría y nos encargaremos de materializar tus espacios!</p>
                    <a href="https://www.google.com" target="_blank" class="btn btn-lg fabrica-button" role="button">CLIC AQUÍ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TESTIMONIAL SECTION END -->*/
@endphp
<!-- BLOG SECTION START -->
<div class="blog-section section pt-100 pb-60">
    <div class="container">
        <div class="row">
            <div class="section-title text-center col-xs-12 mb-70">
                <h4>Últimas tendencias</h4>
                <h2>Nuestro blog</h2>
            </div>
        </div>
        <div class="row">
            @include('content.blog.lista_posts', ['posts' => $posts, 'hasMd' => 1])
        </div>
    </div>
</div>
<!-- BLOG SECTION END -->
@endsection
