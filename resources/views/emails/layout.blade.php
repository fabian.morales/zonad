<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <style>
            body{
                font-family: 'verdana', 'sans-serif';
                font-size: 11px;
                text-align: justify;
                padding: 20px;
            }
            
            body p{
                font-size: 11px;
            }
            
            table img{
                width: 100%;
            }
            
        </style>
    </head>
    <body>
        @yield('content')
        <br />
        <br />
    </body>
</html>