@extends('emails.layout')

@section('content')
    <p>Hola {{ $cotizacion->nombre }}</p><br />
    <p>Hemos recibido tu solicitud de cotizaci&oacute;n. En breve nos pondremos en contacto contigo.</p>
@stop