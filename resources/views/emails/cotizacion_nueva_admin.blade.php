@extends('emails.layout')

@section('content')
    <p>Hola</p><br />
    <p>Se ha recibido una solicitud de cotizaci&oacute;n con los siguientes datos: <br /> 
    Nombre : {{ $cotizacion->nombre }} <br />
    Correo : {{ $cotizacion->correo }} <br />
    Celular : {{ $cotizacion->celular }} <br />
    Ciudad : {{ $cotizacion->ciudad }} <br />
    Producto : {{ $cotizacion->producto->nombre }} <br />
    @if (sizeof($cotizacion->valores))
    <h5>Atributos</h5>
    <ul>
        @foreach($cotizacion->valores as $valor)
        <li>{{ $valor->atributoValor->atributo->nombre }}: {{ $valor->atributoValor->etiqueta }} 
            <span class="color-view" style="background-color: {{ $valor->atributoValor->valor }}"></span>
        </li>
        @endforeach
    </ul>
    @endif
    </p>
@stop