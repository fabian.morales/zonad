@extends('emails.layout')

@section('content')
    <p>Hola</p><br />
    <p>Se ha recibido una solicitud de contacto con los siguientes datos: <br /> 
    Nombre : {{ $contacto->nombre }} <br />
    Correo : {{ $contacto->correo }} <br />
    Tel&eacute;fono : {{ $contacto->telefono }} <br />
    Asunto : {{ $contacto->asunto }} <br />
    Mensaje: {{ $contacto->mensaje }}</p>
@stop