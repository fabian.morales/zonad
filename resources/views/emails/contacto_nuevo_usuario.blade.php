@extends('emails.layout')

@section('content')
    <p>Hola {{ $contacto->nombre }}</p><br />
    <p>Hemos recibido tu solicitud de contacto. En breve nos pondremos en contacto contigo.</p>
@stop