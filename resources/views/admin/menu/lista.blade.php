<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/menus/crear') }}" class="button alert">Item de menu nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de items de menu</div>
</div>
<div class="row item lista head">
    <div class="small-1 columns">N&uacute;m</div>
    <div class="small-4 columns">Nombre</div>
    <div class="small-3 columns">Slug Url</div>
    <div class="small-1 columns">Peso</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($menus as $m)
<div class="row item lista">
    <div class="small-1 columns">{{ $m->id }}</div>
    <div class="small-4 columns">{{ $m->titulo }}</div>
    <div class="small-3 columns">{{ $m->llave }}</div>
    <div class="small-1 columns">{{ $m->peso }}</div>
    <div class="small-2 columns">
        <a class="tooltip-x" title='Editar item' href="{{ url('administrador/menus/editar/'.$m->id) }}"><i class="fi-pencil"></i></a>
        <a class="tooltip-x" title='Borrar item' href="{{ url('administrador/menus/borrar/'.$m->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $menus->render() !!}
    </div>
</div>