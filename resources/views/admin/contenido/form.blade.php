@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de contenidos</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::contenido::guardar') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $pagina->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>T&iacute;titulo</label>
                <input type="text" name="titulo" id="titulo" class="form-control" value="{{ old('titulo') ? old('titulo') : $pagina->titulo }}" required="required" />
                @error('titulo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Ruta</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $pagina->slug }}" required="required" />
                @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-12">
                <label>Cuerpo</label>
                <textarea rows="10" name="cuerpo" id="cuerpo" class="form-control" required="required">
                    {{ old('cuerpo') ? old('cuerpo') : $pagina->cuerpo }}
                </textarea>
                @error('cuerpo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group col-md-1">
                <label>Peso</label>
                <input type="text" name="peso" id="peso" class="form-control" value="{{ old('peso') ? old('peso') : $pagina->peso }}" />
                @error('peso')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-11">&nbsp;</div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="inicio" id="inicio" value="S" @if($pagina->inicio == 'S') checked @endif />
                    Mostrar en el inicio
                </label>
            </div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="activo" id="activo" value="S" @if($pagina->activo == 'S') checked @endif />
                    Activo
                </label>                
            </div>
            
            <div class="col-md-6">
                <a href="{{ route('admin::contenido::index') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-success btn-icon-split float-left" name="guardar_permanecer">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar y permanecer</a>
                </button>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/admin-contenido.js') }}"></script>
@stop
