<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/faqs/crear') }}" class="button alert">FAQ nueva <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de FAQs</div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-7 columns">Nombre</div>
    <div class="small-3 columns">Editar</div>
</div>
@foreach($faqs as $f)
<div class="row item lista">
    <div class="small-2 columns">{{ $f->id }}</div>
    <div class="small-7 columns">{{ $f->titulo }}</div>
    <div class="small-3 columns">
        <a class="tooltip-x" title='Editar FAQ' href="{{ url('administrador/faqs/editar/'.$f->id) }}"><i class="fi-pencil"></i></a>
        <a class="tooltip-x" title='Eliminar FAQ' href="{{ url('administrador/faqs/eliminar/'.$f->id) }}" onclick="return confirm('Desea eliminar esta FAQ?')"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $faqs->render() !!}
    </div>
</div>