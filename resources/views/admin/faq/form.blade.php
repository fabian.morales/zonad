@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos de la FAQ</span></h3>
    </div>
</div>
<form id="form_faq" name="form_faq" action="{{ url('administrador/faqs/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $faq->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Nombre</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="titulo" id="titulo" value="{{ $faq->titulo }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="nombre">Contenido</label>
        </div>
        <div class="small-12 columns">
            <textarea id='descripcion' name='descripcion'>{{ $faq->descripcion }}</textarea>
        </div>        
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">Peso</label>
        </div>
        <div class="small-12 columns">
            <input type="text" name="peso" id="peso" value="{{ $faq->peso }}" />
        </div>
    </div>
    <div class="small-12 columns">
        <label for="activo">
            Activo
            <input type='checkbox' name='activo' id='activo' value='S' @if($faq->activo == 'S') checked="checked" @endif />
        </label>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ url('/administrador/tiendas/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop