@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Solicitudes de contacto</h1>
<p class="mb-4">
    <a href="{{ route('admin::cont::descarga') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-download"></i> Descargar listado</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Registros encontrados</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Tel&eacute;fono</th>
                        <th>Asunto</th>
                        <th>Aceptación de políticas</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contactos as $contacto)
                    <tr>
                        <td>{{ $contacto->id }}</td>
                        <td>{{ $contacto->nombre }}</td>
                        <td>{{ $contacto->telefono }}</td>
                        <td>{{ $contacto->correo }}</td>
                        <td>{{ $contacto->asunto }}</td>
                        <td>{{ $contacto->aceptacion_politicas }}</td>
                        <td>{{ $contacto->created_at }}</td>
                        <td>
                            <span data-toggle="tooltip" data-placement="top" title="Ver mensaje">
                                <a href="#" data-toggle="modal" data-target="#mensaje_contacto_{{ $contacto->id }}">
                                    <i class="fas fa-fw fa-sticky-note"></i>
                                </a>
                            </span>
                            <div class="modal fade" id="mensaje_contacto_{{ $contacto->id }}" tabindex="-1" role="dialog" aria-labelledby="mensaje_{{ $contacto->id }}" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content contact-form">
                                        
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="mensaje_{{ $contacto->id }}" style="float: left; font-size: 2rem; color: #888;">{{ $contacto->asunto }}</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{ $contacto->mensaje }}
                                                
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
