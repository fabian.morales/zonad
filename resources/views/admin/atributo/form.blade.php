@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de atributos de productos</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::atrib::guardar') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $atributo->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') ? old('nombre') : $atributo->nombre }}" required="required" />
                @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Tipo</label>
                @php
                $tipoActual = old('nombre') ? old('nombre') : $atributo->nombre
                @endphp
                <select name="tipo" id="tipo" class="form-control" required="required">
                    @foreach (App\Atributo::obtenerTipos() as $index => $tipo)
                    <option value="{{ $index }}" @if($tipoActual == $index) selected @endif>{{ $tipo }}</option>
                    @endforeach
                </select>
                @error('tipo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>
            <div class="col-md-6">
                <a href="{{ route('admin::atrib::index') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>
@if($atributo->id != 0)
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Valores del atributo</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-3">
                <label>Nombre</label>
                <input type="text" name="etiqueta" id="etiqueta" class="form-control" value="" required="required" />
            </div>
            <div class="form-group col-md-3">
                <label>Valor</label>
                <input type="text" name="valor" id="valor" class="form-control" value="" required="required" />
            </div>
            <div class="form-group col-md-3">
                <br />
                <input type="hidden" id="id_valor" name="id_valor" value="" />
                <button class="btn btn-primary" id="btnGuardarValor">Guardar</button>
            </div>
        </div>
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Valor</th>
                    <th class="no-sort">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr id="tr-valores" class="d-none">
                    <td data-id="id"></td>
                    <td data-id="etiqueta"></td>
                    <td data-id="valor"></td>
                    <td>
                        <a href="{{ route('admin::atrib_valor::mostrar', ['id' => 'X']) }}" class="display-inline" rel="lnk-editar-valor" data-toggle="tooltip" data-placement="top" title="Editar valor de atributo">
                            <i class="fas fa-fw fa-edit"></i>
                        </a>
                        <a href="{{ route('admin::atrib_valor::borrar', ['id' => 'X']) }}" class="display-inline" rel="lnk-borrar-valor" data-del-msg="&iquest;Est&aacute; seguro de borrar este valor de atributo?" data-toggle="tooltip" data-placement="top" title="Borrar valor de atributo">
                            <i class="fas fa-fw fa-trash"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endif

@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/admin-atributo.js') }}"></script>
@stop
