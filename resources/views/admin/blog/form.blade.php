@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de posts</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<form method="post" action="{{ route('admin::blog::guardar') }}" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $post->id }}" />
    @csrf
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>T&iacute;titulo</label>
                    <input type="text" name="titulo" id="titulo" class="form-control" value="{{ old('titulo') ? old('titulo') : $post->titulo }}" required="required" />
                    @error('titulo')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6">&nbsp;</div>

                <div class="form-group col-md-6">
                    <label>Ruta</label>
                    <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $post->slug }}" required="required" />
                    @error('slug')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6">&nbsp;</div>

                <div class="form-group col-md-12">
                    <label>Cuerpo</label>
                    <textarea rows="10" name="cuerpo" id="cuerpo" class="form-control" required="required">
                        {{ old('cuerpo') ? old('cuerpo') : $post->cuerpo }}
                    </textarea>
                    @error('cuerpo')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label>
                        <input type="checkbox" name="activo" id="activo" value="S" @if($post->activo == 'S') checked @endif />
                        Activo
                    </label>                
                </div>
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Categor&iacute;as</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <p>Seleccione las categorías por asignar a este post</p>
                    <br />
                </div>
                @foreach($categorias as $categoria)
                <div class="form-group col-md-4 com-sm-6">
                    <label>
                        <input type="checkbox" name="categorias[]" id="categoria_{{ $categoria->id }}" value="{{ $categoria->id }}" @if(sizeof($categoria->posts) && ($categoria->posts[0]->id == $post->id)) checked @endif />
                        {{ $categoria->nombre }}
                    </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Imagen</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Imagen</label>
                    <input type="file" name="imagen" id="imagen" />
                </div>
                <div class="col-md-6">
                    @if(!empty($post->imagen))
                        <img width="200" src="{{ asset($post->getOriginalImageUrl()) }}" alt="Imagen" />
                    @else
                        &nbsp;
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('admin::blog::index') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Regresar</a>
            </a>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-success btn-icon-split float-left" name="guardar_permanecer">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar y permanecer</a>
            </button>
            <div class="float-left">&nbsp;</div>
            <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar</a>
            </button>
        </div>
    </div>
</form>
@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/admin-contenido.js') }}"></script>
@stop
