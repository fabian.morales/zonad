<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('administrador/galerias/crear') }}" class="button alert">Galer&iacute;a nueva <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de galer&iacute;as</div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-5 columns">Nombre</div>
    <div class="small-3 columns">Token</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($galerias as $g)
<div class="row item lista">
    <div class="small-2 columns">{{ $g->id }}</div>
    <div class="small-5 columns">{{ $g->titulo }}</div>
    <div class="small-5 columns">{{ str_replace('\\', '', '\\{!\\! \\$\\formulario|'.$g->llave.' \\!\\!}') }}</div>
    <div class="small-2 columns">
        <a class="tooltip-x" title='Editar Galer&iacute;a' href="{{ url('administrador/galerias/editar/'.$g->id) }}"><i class="fi-pencil"></i></a>
        <a rel="borrar-galeria" class="tooltip-x" title='Borrar galer&iacute;a' href="{{ url('administrador/galerias/borrar/'.$g->id) }}"><i class="fi-trash"></i></a>
    </div>
</div>
@endforeach
