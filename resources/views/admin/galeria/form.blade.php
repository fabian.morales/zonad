@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Datos de la galería</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos principales</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::gal::guardar') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $galeria->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="titulo" id="titulo" class="form-control" value="{{ old('titulo') ? old('titulo') : $galeria->titulo }}" required="required" />
                @error('titulo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Llave</label>
                <input type="text" name="llave" id="llave" class="form-control" value="{{ old('llave') ? old('llave') : $galeria->llave }}" required="required" />
                @error('llave')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Tipo</label>
                <select name="tipo" id="tipo" class="form-control" required="required">
                    <option value="banner" @if($galeria->tipo == 'banner') selected @endif>Banner</option>
                    <option value="mosaico" @if($galeria->tipo == 'mosaico') selected @endif>Mosaico</option>
                </select>
                @error('tipo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>
            
            <div class="col-md-6">
                <a href="{{ route('admin::gal::index') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-success btn-icon-split float-left" name="guardar_permanecer">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar y permanecer</a>
                </button>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>

@if($galeria->id)
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Imagenes</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <a id="btnFotoNueva" class="btn btn-primary float-right" href="">
                    <i class="fas fa-fw fa-plus-square"></i> Agregar foto
                </a>
                <br />
            </div>
        </div>

        <div id="fotos_galeria" class="row">
        @foreach($galeria->fotos as $f)
            <div class="col-md-3">
                <div class="item-galeria relativo">
                    <img class="img-responsive" src="{{ asset('storage/imagenes/galeria/' . $f->galeria_id . '/' . $f->archivo) }}" alt="{{ $f->titulo }}" title="{{ $f->titulo }}" />
                    <div class="acciones">
                        <a rel="btnBorrarFoto" href="{{ route('admin::galfoto::borrar', ['id' => $f->id]) }}"><i class="fas fa-fw fa-trash"></i></a>
                        <a rel="btnEditarFoto" href="{{ route('admin::galfoto::editar', ['id' => $f->id]) }}"><i class="fas fa-fw fa-edit"></i></a>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>

<div class="fade" id="item-source" data-url="{{ url('storage/imagenes/galeria/' . $galeria->id) }}">
    <div class="col-md-3">
        <div class="item-galeria relativo">
            <img class="img-responsive" src="" alt="" title="" />
            <div class="acciones">
                <a rel="btnBorrarFoto" href="{{ route('admin::galfoto::borrar', ['id' => 'xxx']) }}"><i class="fas fa-fw fa-trash"></i></a>
                <a rel="btnEditarFoto" href="{{ route('admin::galfoto::editar', ['id' => 'xxx']) }}"><i class="fas fa-fw fa-edit"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="foto_modal" tabindex="-1" role="dialog" aria-labelledby="foto_modal_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="foto_modal_label">Agregar una nueva imagen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_foto" name="form_foto" class="form-row" action="{{ route('admin::galfoto::guardar') }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="id_foto" name="id" value="" />
                    <input type="hidden" id="galeria_id" name="galeria_id" value="{{ $galeria->id }}" />
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                    <div class="col-sm-12 form-group">
                        <label for="titulo">Nombre</label>
                        <input type="text" name="titulo" id="titulo_foto" class="form-control" value="" />
                    </div>

                    <div class="col-sm-12 form-group">
                        <label for="descripcion_foto">Descripci&oacute;n</label>
                        <textarea id='descripcion_foto' name='descripcion' class="form-control" rows="3"></textarea>
                    </div>

                    <div class="col-sm-12 form-group">
                        <label for="imagen">Archivo de imagen</label>
                        <input type="file" id="imagen" name="imagen" class="form-control" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                <a id="btnGuardarFoto" class="btn btn-primary btn-icon-split float-left" href="">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/admin-galeria.js') }}"></script>
@stop
