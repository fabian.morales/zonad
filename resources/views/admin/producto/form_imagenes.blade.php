<div id="imagenes_drop">
    <div class="dz-message">Arrastra aqu&iacute; las im&aacute;genes o haz clic</div>
</div>

@if (!empty($producto->imagenes))
<div class="col-sm-6 col-md-3 col-lg-2 item_img_adm" id="item_img" style="display: none;">
    <input type="checkbox" name="borrar_img[]" id="borrar_img" value="" />
    <img src="" alt="{{ $producto->nombre }}" class="img-responsive" />
</div>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Im&aacute;genes asignadas</h6>
    </div>
    <div class="card-body">
        <div id="galeria_imagenes" class="row">
            @foreach($producto->imagenes as $imagen)
            <div class="col-sm-6 col-md-3 col-lg-2 item_img_adm">
                <input type="checkbox" name="borrar_img[]" id="borrar_img_{{ $imagen->id }}" value="{{ $imagen->id }}" />
                <img src="{{ url($imagen->getThumbUrl()) }}" alt="{{ $producto->nombre }}" class="img-responsive" />
            </div>
            @endforeach
        </div>
        <button type="submit" class="btn btn-danger btn-icon-split float-left" id="btnBorrarImg">
            <span class="icon text-white-50">
                <i class="fas fa-trash"></i>
            </span>
            <span class="text">Borrar im&aacute;genes seleccionadas</a>
        </button>
    </div>
</div>
<div class="clearfix"></div>
<br />
@else
<p>Debe guardar primero el producto para poder asignar las im&aacute;genes</p>
<br />
@endif