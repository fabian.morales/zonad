@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de productos</h1>
<p class="mb-4">
    &nbsp;
</p>

<form method="post" action="{{ route('admin::prod::guardar') }}" class="" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $producto->id }}" />
    @csrf

    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-datos-tab" data-toggle="tab" href="#nav-datos" role="tab" aria-controls="nav-datos" aria-selected="true">Datos b&aacute;sicos</a>
            <a class="nav-item nav-link" id="nav-categorias-tab" data-toggle="tab" href="#nav-categorias" role="tab" aria-controls="nav-categorias" aria-selected="false">Categor&iacute;as</a>
            <a class="nav-item nav-link" id="nav-imagenes-tab" data-toggle="tab" href="#nav-imagenes" role="tab" aria-controls="nav-imagenes" aria-selected="false">Im&aacute;genes</a>
            <a class="nav-item nav-link" id="nav-atributos-tab" data-toggle="tab" href="#nav-atributos" role="tab" aria-controls="nav-atributos" aria-selected="false">Atributos</a>
            <a class="nav-item nav-link" id="nav-ficha-tab" data-toggle="tab" href="#nav-ficha" role="tab" aria-controls="nav-ficha" aria-selected="false">Ficha t&eacute;cnica</a>
            <a class="nav-item nav-link" id="nav-oferta-tab" data-toggle="tab" href="#nav-oferta" role="tab" aria-controls="nav-oferta" aria-selected="false">Oferta</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-datos" role="tabpanel" aria-labelledby="nav-datos-tab">
            @include('admin.producto.form_datos', ['producto' => $producto])
        </div>
        <div class="tab-pane fade" id="nav-categorias" role="tabpanel" aria-labelledby="nav-categorias-tab">
            @include('admin.producto.form_categoria', ['producto' => $producto, 'categorias' => $categorias])
        </div>
        <div class="tab-pane fade" id="nav-imagenes" role="tabpanel" aria-labelledby="nav-imagenes-tab">
            @include('admin.producto.form_imagenes', ['producto' => $producto])
        </div>
        <div class="tab-pane fade" id="nav-atributos" role="tabpanel" aria-labelledby="nav-atributos-tab">
            @include('admin.producto.form_atributos', ['producto' => $producto, 'atributos' => $atributos])
        </div>
        <div class="tab-pane fade" id="nav-ficha" role="tabpanel" aria-labelledby="nav-ficha-tab">
            @include('admin.producto.form_ficha', ['producto' => $producto, 'caracteristicas' => $caracteristicas])
        </div>
        <div class="tab-pane fade" id="nav-oferta" role="tabpanel" aria-labelledby="nav-oferta-tab">
            @include('admin.producto.form_oferta', ['producto' => $producto])
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('admin::prod::index') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Regresar</a>
            </a>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-success btn-icon-split float-left" name="guardar_permanecer">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar y permanecer</a>
            </button>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-primary btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar</a>
            </button>
        </div>
    </div>
</form>
@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/dropzone.js') }}"></script>
<script src="{{ asset('js/admin-producto.js') }}"></script>
@stop
