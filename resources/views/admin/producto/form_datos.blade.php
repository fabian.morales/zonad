<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') ? old('nombre') : $producto->nombre }}" required="required" />
                @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>C&oacute;digo</label>
                <input type="text" name="codigo" id="codigo" class="form-control" value="{{ old('codigo') ? old('codigo') : $producto->codigo }}" required="required" />
                @error('codigo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Ruta</label>
                <input type="text" name="slug" id="nombre" class="form-control" value="{{ old('slug') ? old('slug') : $producto->slug }}" required="required" />
                @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Descripci&oacute;n</label>
                <textarea name="descripcion" id="descripcion" class="form-control" required="required" rows="5">{{ old('descripcion') ? old('descripcion') : $producto->descripcion }}</textarea>
                @error('descripcion')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="destacado" id="destacado" value="S" @if($producto->destacado == 'S') checked @endif />
                    Destacado
                </label>
            </div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="activo" id="activo" value="S" @if($producto->activo == 'S') checked @endif />
                    Activo
                </label>                
            </div>
            @php
            /*<div class="col-md-6">
                <a href="{{ route('admin::prod::index') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>*/
            @endphp
        </div>
    </div>
</div>