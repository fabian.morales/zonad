<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Atributos</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-sm-12">
                <p>Seleccione la combinaci&oacute;n de atributos que desee a&ntilde;adir</p>
            </div>
            @foreach($atributos as $atributo)
            <div class="form-group col-md-4 com-sm-6">
                <fieldset>
                    <legend>{{ $atributo->nombre }}</legend>
                    <ul class="list">
                        @foreach($atributo->valores as $valor)
                        <li>
                            <label>
                                <input rel="combinacion" type="radio" value="{{ $valor->id }}" id="atributo_valor_{{ $valor->id }}" name="atributo_valor[{{ $valor->id }}]" data-atributo="{{ $atributo->id }}" /> {{ $valor->etiqueta }}
                            </label>
                        </li>
                        @endforeach
                    </ul>
                </fieldset>
            </div>
            @endforeach
            <div class="col-md-12">
                <button id="btnGuardarAtributo" class="btn btn-primary">A&ntilde;adir combinaci&oacute;n</button>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Atributos del producto</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Atributo</th>
                        <th>Valor</th>
                        <th class="no-sort">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="tr-valores" class="d-none">
                        <td data-id="id"></td>
                        <td data-id="atributo"></td>
                        <td data-id="valor"></td>
                        <td>
                            <a href="{{ route('admin::prod_atrib::borrar', ['id' => 'X']) }}" class="display-inline" rel="lnk-borrar-atributo" data-del-msg="&iquest;Est&aacute; seguro de borrar este atributo del producto?" data-toggle="tooltip" data-placement="top" title="Borrar atributo">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>