<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Ficha t&eacute;cnica</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            @foreach($caracteristicas as $caracteristica)
            <div class="col-md-2">
                <label>{{ $caracteristica->nombre }}</label>
            </div>
            <div class="form-group col-md-6">                
                <input type="text" name="caracteristicas[{{ $caracteristica->id }}]" id="caracteristica_{{ $caracteristica->id }}" class="form-control" @if($caracteristica->relationLoaded('valores') && sizeof($caracteristica->valores)) value="{{ $caracteristica->valores[0]->valor }}" @else value="" @endif />
            </div>
            <div class="col-md-4">&nbsp;</div>
            @endforeach
        </div>
    </div>
</div>