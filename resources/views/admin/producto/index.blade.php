@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Productos</h1>
<p class="mb-4">
    <a href="{{ route('admin::prod::nuevo') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> Crear producto</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Productos registradas</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Imagen</th>
                        <th>Destacado</th>
                        <th>Activo</th>
                        <th class="no-sort">Acciones</th>
                    </tr>
                </thead>
                @php
                /*<tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Destacado</th>
                        <th>Activo</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>*/
                @endphp
                <tbody>
                    @foreach($productos as $producto)
                    <tr>
                        <td>{{ $producto->id }}</td>
                        <td>{{ $producto->nombre }}</td>
                        <td class="text-center">@if(sizeof($producto->imagenes) && is_file($producto->imagenes[0]->getPath())) <img src="{{ asset($producto->imagenes[0]->getThumbUrl()) }}" width="35" /> @endif</td>
                        <td>{{ $producto->destacado == 'S' ? 'Si' : 'No' }}</td>
                        <td>{{ $producto->activo == 'S' ? 'Si' : 'No' }}</td>
                        <td>
                            <a href="{{ route('admin::prod::editar', ['id' => $producto->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Editar producto">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>

                            <a href="{{ route('admin::prod::borrar', ['id' => $producto->id]) }}" rel="lnk-borrar" data-del-msg="&iquest;Est&aacute; seguro de borrar este producto?" class="display-inline" data-toggle="tooltip" data-placement="top" title="Borrar producto">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
