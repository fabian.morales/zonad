<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Oferta</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="oferta" id="oferta" value="S" @if($producto->oferta == 'S') checked @endif />
                    Habilitar producto como oferta
                </label>
            </div>

            <div class="form-group col-md-6">
                <label>Descripci&oacute;n de la oferta</label>
                <textarea name="descripcion_oferta" id="descripcion_oferta" class="form-control"  rows="5">{{ $producto->descripcion_oferta }}</textarea>
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Imagen de la oferta</label>
                <input type="file" name="archivo_oferta" id="archivo_oferta" />
            </div>
            <div class="col-md-6">
                @if(!empty($producto->archivo_oferta))
                    <img width="200" src="{{ url('storage/imagenes/productos/' . $producto->id . '/oferta/' . $producto->archivo_oferta) }}" alt="Oferta" />
                @else
                    &nbsp;
                @endif
            </div>
        </div>
    </div>
</div>