<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Categor&iacute;as</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-sm-12">
                <p>Seleccione las categor&iacute;as por asignar a este producto</p>
                <br />
            </div>
            @foreach($categorias as $categoria)
            <div class="form-group col-md-4 com-sm-6">
                <label>
                    <input type="checkbox" name="categorias[]" id="categoria_{{ $categoria->id }}" value="{{ $categoria->id }}" @if(sizeof($categoria->productos) && ($categoria->productos[0]->id == $producto->id)) checked @endif data-name="{{ $categoria->nombre }}" data-main="{{ (int)(isset($principal) && !empty($principal) && $principal->id == $categoria->id) }}" />
                    {{ $categoria->nombre }}
                </label>
            </div>
            @endforeach
        </div>
        <div class="form-row">
            <div class="col-sm-6 col-md-3">
                <p>Seleccione la categor&iacute;a principal</p>
                <br />
            </div>
            <div class="form-group col-md-3 com-sm-6">
                <label>
                    <select id="categoria_principal" name="categoria_principal">
                    </select>
                </label>
            </div>
        </div>
    </div>
</div>