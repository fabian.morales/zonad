@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Ver cotizaci&oacute;n</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos del cliente</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::cot::guardar') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $cotizacion->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Fecha</label>
                <input type="text" name="fecha" id="fecha" class="form-control" value="{{ $cotizacion->created_at }}" required="required" readonly="readonly" />
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ $cotizacion->nombre }}" required="required" readonly="readonly" />
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Correo</label>
                <input type="email" name="correo" id="correo" class="form-control" value="{{ $cotizacion->correo }}" required="required" readonly="readonly" />
            </div>
            <div class="form-group col-md-6">
                <label>Celular</label>
                <input type="email" name="celular" id="celular" class="form-control" value="{{ $cotizacion->celular }}" required="required" readonly="readonly" />
            </div>

            <div class="form-group col-md-6">
                <label>Ciudad</label>
                <input type="text" name="ciudad" id="ciudad" class="form-control" value="{{ $cotizacion->ciudad }}" required="required" readonly="readonly" />
            </div>
            <div class="form-group col-md-6">
                <label>Whatsapp</label>
                <input type="text" name="whatsapp" id="whatsapp" class="form-control" value="{{ $cotizacion->whatsapp == 'S' ? 'Sí': 'No' }}" required="required" readonly="readonly" />
            </div>
            
            <div class="col-md-6">
                <a href="{{ route('admin::cot::index') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-success btn-icon-split float-left" name="guardar_permanecer">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar y permanecer</a>
                </button>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Producto</h6>
    </div>
    <div class="card-body">
        @php
            $img = asset('/front/img/product.png');
            if (sizeof($cotizacion->producto->imagenes)) {
                $img = asset($cotizacion->producto->imagenes[0]->getThumbUrl());
            }
        @endphp
        <div class="row">
            <div class="col-sm-6 col-md-2">
                <img src="{{ $img }}" class="img-fluid" alt="" />
            </div>
            <div class="col-sm-6 col-md-10">
                <h4>{{ $cotizacion->producto->nombre }}</h4>
                <hr />
                @if (sizeof($cotizacion->valores))
                    <h5>Atributos</h5>
                    <ul>
                        @foreach($cotizacion->valores as $valor)
                        <li>{{ $valor->atributoValor->atributo->nombre }}: {{ $valor->atributoValor->etiqueta }} 
                            <span class="color-view" style="background-color: {{ $valor->atributoValor->valor }}"></span>
                        </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Mensajes</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h5>Mensaje del cliente</h5>
                <textarea readonly="readonly" rows="5" class="form-control">{{ $cotizacion->mensaje }}</textarea>
            </div>
            <div class="col-sm-12">
                <hr />
                <h5>Responder</h5>
                <textarea class="form-control" rows="5"></textarea>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/admin-contenido.js') }}"></script>
@stop
