@extends('layouts.master_admin')

@section('css')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Cotizaciones</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Cotizaciones registradas</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Url</th>
                        <th>Fecha creaci&oacute;n</th>
                        <th class="no-sort">Acciones</th>
                    </tr>
                </thead>
                @php
                /*<tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>*/
                @endphp
                <tbody>
                    @foreach($cotizaciones as $cotizacion)
                    <tr>
                        <td>{{ $cotizacion->id }}</td>
                        <td>{{ $cotizacion->nombre }}</td>
                        <td>{{ $cotizacion->correo }}</td>
                        <td>{{ $cotizacion->created_at }}</td>
                        <td>
                            <a href="{{ route('admin::cot::editar', ['id' => $cotizacion->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Ver cotizaci&oacute;n">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>

                            <a href="{{ route('admin::cot::borrar', ['id' => $cotizacion->id]) }}" class="display-inline" rel="lnk-borrar" data-del-msg="&iquest;Est&aacute; seguro de borrar esta cotizaci&oacute;n?" data-toggle="tooltip" data-placement="top" title="Borrar cotizaci&oacute;n">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
