(function ($) {
    "use strict";
    function initialize() {
        var mapOptions = {
            zoom: 12,
            scrollwheel: false,
            center: new google.maps.LatLng(3.412497, -76.533616)
        };

        var map = new google.maps.Map(document.getElementById('contact-map'),
            mapOptions);

        /*var marker = new google.maps.Marker({
            position: map.getCenter(),
            map: map
        });*/

        var surLatLng = {lat: 3.3644304, lng: -76.5317276};
        var markerSur = new google.maps.Marker({
            position: surLatLng,
            icon: $("#logo-map").attr("src"),
            map: map
        });

        var norteLatLng = {lat: 3.462097, lng: -76.531344};
        var markerNorte = new google.maps.Marker({
            position: norteLatLng,
            icon: $("#logo-map").attr("src"),
            map: map
        });

        var styles = [{
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#8d8d8d"
                }]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [{
                    "color": "#f5f5f5"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [{
                    "visibility": "simplified"
                }]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{
                        "color": "#dbdbdb"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]
        map.setOptions({
            styles: styles
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
})(jQuery);
