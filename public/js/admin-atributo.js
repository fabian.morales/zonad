(function(w, $) {
    $(document).ready(function() {
        function hookLinks() {
            $('a[rel=lnk-borrar-valor]').off('click');
            $('a[rel=lnk-borrar-valor]').click(function(e) {
                e.preventDefault();
                if (!confirm($(this).attr('data-del-msg'))){
                    return;
                }

                $.ajax({
                    url: $(this).attr('href'),
                    method: 'get',
                    dataType: 'json'
                })
                .done(function(data) {
                    if (data.ok === 1) {
                        mostrarValores(data.datos);
                        hookLinks();
                        alert('Valor de atributo borrado exitosamente');
                    }
                    else{
                        alert('No se pudo borrar el valor de atributo');
                    }
                });
            });

            $('a[rel=lnk-editar-valor]').off('click');
            $('a[rel=lnk-editar-valor]').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('href'),
                    method: 'get',
                    dataType: 'json'
                })
                .done(function(data) {
                    if (data.ok === 1) {
                        $('#id_valor').val(data.datos.id);
                        $('#etiqueta').val(data.datos.etiqueta);
                        $('#valor').val(data.datos.valor);
                    }
                    else{
                        alert('No se pudo obtener los datos del valor de atributo');
                    }
                });
            });
        }

        $('#btnGuardarValor').click(function(e) {
            e.preventDefault();
            if ($('#etiqueta').val() === '') {
                alert('El nombre del valor es obligatorio');
                return;
            }

            if ($('#valor').val() === '') {
                alert('El valor es obligatorio');
                return;
            }

            $.ajax({
                url:  w.frontier_admin.base_url + '/admin/atributo/valores/guardar',
                data: {
                    'id' : $('#id_valor').val(),
                    'etiqueta' : $('#etiqueta').val(),
                    'valor' : $('#valor').val(),
                    'atributo_id': $('#id').val(),
                    '_token': $('input[name="_token"]').val()
                },
                method: 'post',
                dataType: 'json'
            })
            .done(function(data) {
                if (data.ok === 1) {
                    $('#id_valor').val('');
                    $('#etiqueta').val('');
                    $('#valor').val('');
                    mostrarValores(data.datos);
                    hookLinks();
                    alert('Valor de atributo guardado exitosamente');
                }
                else{
                    alert('No se pudo guardar el valor de atributo');
                } 
            });
        });

        function mostrarValores(data) {
            let $trBase = $('#tr-valores').clone();
            let $contenedor = $('#tr-valores').parent()
            $contenedor.html('').append($trBase);
            $.each(data, function(i, o) {
                let $tr = $trBase.clone();
                $tr.attr('id', 'tr_' + Math.random());
                $tr.find('td[data-id=id]').text(o.id);
                $tr.find('td[data-id=etiqueta]').text(o.etiqueta);
                $tr.find('td[data-id=valor]').text(o.valor);
                let $lnkBorrar = $tr.find('a[rel=lnk-borrar-valor]');
                $lnkBorrar.attr('href', $lnkBorrar.attr('href').replace('X', o.id));
                let $lnkEditar = $tr.find('a[rel=lnk-editar-valor]');
                $lnkEditar.attr('href', $lnkEditar.attr('href').replace('X', o.id));
                $tr.removeClass('d-none');
                $('#tr-valores').parent().append($tr);
            });
        }

        function recuperarValores() {
            if ($('#id').val() !== '') {
                $.ajax({
                    url:  w.frontier_admin.base_url + '/admin/atributo/' + $('#id').val() + '/valores',
                    method: 'get',
                    dataType: 'json'
                })
                .done(function(data) {
                    mostrarValores(data);
                    hookLinks();
                });
            }
        }

        recuperarValores();
    });
})(window, jQuery);