(function(w, $){
    $(document).ready(function() {
        tinymce.init({
            selector: "textarea#cuerpo",
            //content_css : 'css/editor.css',
            height: 400,
            theme: 'silver',
            language: 'es',
            relative_urls: false,
            document_base_url: w.frontier_admin.base_url,
            remove_script_host: false,
            extended_valid_elements: 'span,iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor ",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | visualblocks | table",
            image_advtab: true
        });
        
        $("#btnAgregarVar").click(function(e) {
            e.preventDefault();
            tinymce.activeEditor.execCommand('mceInsertContent', false, $("#variable").val());
        });
    });
})(window, jQuery);