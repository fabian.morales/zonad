(function(w, $) {
    function hookLinks() {
        $("a[rel='btnEditarFoto']").click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('href'),
                type: 'get',
                dataType: 'json'
            })
            .done(function(data) {
                $("#form_foto")[0].reset();
                $("#id_foto").val(data.id);
                $("#titulo_foto").val(data.titulo);
                $("#descripcion_foto").val(data.descripcion);
                $("#foto_modal").modal();
            });
        });

        $("a[rel='btnBorrarFoto']").click(function(e) {
            e.preventDefault();
            if (!confirm('¿Está seguro de eliminar esta foto?')){
                return;
            }

            $.ajax({
                url: $(this).attr('href'),
                type: 'get',
                dataType: 'json'
            })
            .done(function(data) {
                if (data.ok == 1) {
                    mostrarGaleria(data.fotos);
                    hookLinks();
                }
                else{
                    alert(data.mensaje);
                }
            });
        });
    }

    function mostrarGaleria(fotos) {
        $("#fotos_galeria").html('');
        $.each(fotos, function(i, o) {
            var url = $("#item-source").attr("data-url");
            var $parent = $("#item-source > div").clone();
            $parent.find('img').attr('src', url + '/' + o.archivo);
            var $lnkBorrar = $parent.find("a[rel='btnBorrarFoto']");
            var hrefAnt = $lnkBorrar.attr('href');
            $lnkBorrar.attr('href', hrefAnt.replace('xxx', o.id));
            var $lnkEditar = $parent.find("a[rel='btnEditarFoto']");
            hrefAnt = $lnkEditar.attr('href');
            $lnkEditar.attr('href', hrefAnt.replace('xxx', o.id));
            $("#fotos_galeria").append($parent);
        });
    }

    $(document).ready(function() {
        $("#btnFotoNueva").click(function(e) {
            e.preventDefault();
            $("#form_foto")[0].reset();
            $("#id_foto").val('');
            $("#foto_modal").modal();
        });
        
        $("#btnGuardarFoto").click(function(e) {
            e.preventDefault();
            var $form = $("#form_foto");
            var formData = new FormData(document.getElementById("form_foto"));
            $.ajax({
                url: $form.attr("action"),
                type: "post",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
	            processData: false
            })
            .done(function(res){
                if (res.ok == 1) {
                    mostrarGaleria(res.fotos);
                    hookLinks();
                    $("#foto_modal").modal('hide');
                }
                else{
                    alert(res.mensaje);
                }
            });
        });

        hookLinks();
    });
})(window, jQuery);