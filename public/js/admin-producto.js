(function(w, $) {
    $(document).ready(function() {
        function hookImagenes() {
            $('#galeria_imagenes .img-responsive').click(function(e) {
                e.preventDefault();
                var $check = $(this).parent().find('input[type="checkbox"]');
                $check.prop('checked', !$check.prop('checked'));
            });
        }
        
        function poblarImagenes(json) {
            $("#galeria_imagenes").html('');
            $.each(json, function(i, o) {
                var $div = $('#item_img').clone();
                $div.attr('id', 'item_img_' + o.id);
                $div.show();
                var $check = $div.find('input[type=checkbox]');
                $check.attr('id', 'borrar_img_' + o.id);
                $check.val(o.id);
                var $img = $div.find('img');
                $img.attr('src', w.frontier_admin.base_url + '/storage/imagenes/productos/' + o.producto_id + '/' + o.archivo)
                $('#galeria_imagenes').append($div);
            });

            hookImagenes();
        }

        var dropzone = $("#imagenes_drop").dropzone({ 
            url:  w.frontier_admin.base_url + "/admin/producto/imagenes/" + $("#id").val(),
            params: {
                "_token": $("input[name='_token']").val()
            },
            acceptedFiles: 'image/*',
            complete: function(data) {
                poblarImagenes(JSON.parse(data.xhr.response));
            }
        });

        $("#btnBorrarImg").click(function(e) {
            e.preventDefault();
            if (confirm('¿Está seguro de borrar las imágenes seleccionadas?')) {
                $.ajax({
                    url: w.frontier_admin.base_url + '/admin/producto/imagenes/' + $("#id").val() + '/borrar',
                    method: 'post',
                    data: $('#galeria_imagenes input[type="checkbox"]:checked, input[name="_token"]'),
                    dataType: 'json'
                })
                .done(function(data) {
                    poblarImagenes(data);
                });
            }
        });

        function hookLinksAtributos() {
            $('a[rel=lnk-borrar-atributo]').off('click');
            $('a[rel=lnk-borrar-atributo]').click(function(e) {
                e.preventDefault();
                if (!confirm($(this).attr('data-del-msg'))){
                    return;
                }

                $.ajax({
                    url: $(this).attr('href'),
                    method: 'get',
                    dataType: 'json'
                })
                .done(function(data) {
                    if (data.ok === 1) {
                        mostrarAtributos(data.datos);
                        hookLinksAtributos();
                        alert('Atributo borrado exitosamente');
                    }
                    else{
                        alert('No se pudo borrar el atributo');
                    }
                });
            });
        }

        $('#btnGuardarAtributo').click(function(e) {
            e.preventDefault();

            if ($('input[rel="combinacion"]:checked').length === 0) {
                alert('Debe seleccionar un valor');
                return;
            }

            var idValor = $('input[rel="combinacion"]:checked').val();
            var idAtributo = $('input[rel="combinacion"]:checked').attr('data-atributo');

            $.ajax({
                url:  w.frontier_admin.base_url + '/admin/producto/atributos/guardar',
                data: {
                    'id_atributo' : idAtributo,
                    'id_valor' : idValor,
                    'id_producto': $('#id').val(),
                    '_token': $('input[name="_token"]').val()
                },
                method: 'post',
                dataType: 'json'
            })
            .done(function(data) {
                if (data.ok === 1) {
                    mostrarValores(data.datos);
                    hookLinksAtributos();
                    alert('Valor de atributo guardado exitosamente');
                }
                else{
                    alert('No se pudo guardar el valor de atributo');
                } 
            });
        });

        function mostrarValores(data) {
            let $trBase = $('#tr-valores').clone();
            let $contenedor = $('#tr-valores').parent()
            $contenedor.html('').append($trBase);
            $.each(data, function(i, o) {
                let $tr = $trBase.clone();
                $tr.attr('id', 'tr_' + Math.random());
                $tr.find('td[data-id=id]').text(o.id);
                $tr.find('td[data-id=atributo]').text(o.atributo.nombre);
                $tr.find('td[data-id=valor]').text(o.etiqueta);
                let $lnkBorrar = $tr.find('a[rel=lnk-borrar-atributo]');
                $lnkBorrar.attr('href', $lnkBorrar.attr('href').replace('X', o.id));
                $tr.removeClass('d-none');
                $('#tr-valores').parent().append($tr);
            });
        }

        function recuperarValores() {
            if ($('#id').val() !== '') {
                $.ajax({
                    url:  w.frontier_admin.base_url + '/admin/producto/' + $('#id').val() + '/atributos',
                    method: 'get',
                    dataType: 'json'
                })
                .done(function(data) {
                    mostrarValores(data);
                    hookLinksAtributos();
                });
            }
        }

        let insertarCategoriasSel = () => {
            $("#categoria_principal").html('');
            $("input[name='categorias[]']").filter((i, o) => o.checked).each((i, o) => {
                let $option = $(`<option value="${$(o).val()}">${$(o).attr('data-name')}</option>`);
                $option.prop("selected", $(o).attr("data-main") == 1);
                $("#categoria_principal").append($option);
            });
        };

        $("input[name='categorias[]']").on('change', () => {
            insertarCategoriasSel();
        });

        insertarCategoriasSel();
        recuperarValores();
        hookImagenes();
    });
})(window, jQuery);